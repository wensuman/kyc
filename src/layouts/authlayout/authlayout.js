import React from 'react'
import Cookie from 'react-cookie'
import IsLoggedIn from '../../modules/sessions'

export class AuthLayout extends React.Component{
  constructor(props){
    super(props)
  }
  render(){

    // if(IsLoggedIn()){
    //   this.props.router.push('/')
    //   return (
    //     <div>
    //       Redirecting ...
    //     </div>
    //   )
    // }
  return(
    <div>
      {this.props.children}
    </div>
  )
  }
}


AuthLayout.propTypes = {
  children : React.PropTypes.element.isRequired
}

export default AuthLayout
