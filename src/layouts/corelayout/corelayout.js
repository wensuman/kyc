import React from 'react'
import Nav from '../../components/nav'
import Breadcrumb from '../../components/breadcrumb'
import Sidebar from '../../components/sidebar'
import Navbar from '../../components/nav'
import PageTransition from 'react-router-page-transition'
import IsLoggedIn from '../../modules/sessions'
import Ajax from '../../modules/request'
import {Urls} from '../../config/app'
import '../../styles/table.css'
import '../../styles/button.css'
import Redirect from '../../components/redirect'

export class CoreLayout extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      profile: ''
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount() {
    Ajax().get(Urls.profile).then((response) => {
      this.setState({profile: response.data.data})
    });
    (function() {
      $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();
      $('html').addClass('perfect-scrollbar-on');
    })();
  }

  render() {
    if (!IsLoggedIn()) {
      this.props.router.push('/login?session_expired=true')
      return <Redirect/>;
    }
    return (
      <div className="animated fadeIn">
        <div className="wrapper">
          <Sidebar router={this.props.router}/>
          <div className="main-panel">
            <Navbar bread={this.props.router.location.pathname.split('/').join(' ')} router={this.props.router}/>
            <div className="content">
              <div className="container-fluid">
                <div className="row">
                  <div className="col-md-12" style={{position:'relative',height: '100%'}}>
                    {this.props.children}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

CoreLayout.propTypes = {
  children: React.PropTypes.element.isRequired
}

export default CoreLayout
