import React from 'react';
import Ajax from '../../../modules/request';
import {Urls} from '../../../config/app';
import Cookie from 'react-cookie';
import './css/style.css'
import {IndexLink, Link} from 'react-router'

export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      phone_number: '',
      remember_me: 'on',
      password: '',
      register_phone_number: '',
      register_password: '',
      register_email: '',
      register_password_confirmation: '',
      verification_code: '',
      email: '',
      verified: false,
      login: '',
      ajaxHappening: '',
      register_agree: false,
      displayLoginError: false,
      displaySignUpError: false,
      displayVerifyError: false,
      initial: true
    };
    this.change = this.change.bind(this)
    this.allowLogin = this.allowLogin.bind(this)
    this.allowSignUp = this.allowSignUp.bind(this)
    this.showLogin = this.showLogin.bind(this)
    this.validEmail = this.validEmail.bind(this)
    this.verify = this.verify.bind(this)
  }

  change(name, event) {
    if (event.target.type == 'checkbox') {
      this.state[name] == 'on'
        ? event.target.value = ''
        : event.target.value = 'on';
    } else if (name.includes('number')) {
      event.target.value = event.target.value.replace(/\D/g, '')
    }
    this.setState({[name]: event.target.value})
  }

  login(event) {
    event.preventDefault()
    if(!this.state.phone_number){
      alert('Please enter your mobile number','error');
      return false;
    }
    if(this.state.phone_number.length != 10){
      alert('Mobile Number should be exactly of 10 digit','error');
      return false;
    }
    this.setState({logging: true});
    Ajax().post(Urls.login, this.state).then((response) => {
      this.setState({logging:false})
      if (response.status === 202) {
        Cookie.save('Authorization', 'Bearer ' + response.data.data.token,{maxAge: 7200})
        this.props.router.push('/template')
        alert('Login Successfull')
      }
      if (response.response.status === 401) {
        alert('Login was not recognized by the system','error')
      }
    }).catch((error) => {
      this.setState({logging:false})
    })
  }

  register(event) {
    event.preventDefault()
    if(this.state.registering){
      return;
    }
    if(!this.state.register_citizenship_no){
      alert('Please enter citizenship number.','error');
      return;
    }
    if(!this.state.register_phone_number || this.state.register_phone_number.length != 10){
      alert('Please enter your mobile number of 10 digits.','error');
      return;
    }
    if(!this.state.register_password){
      alert('Please enter a password.','error')
      return;
    }
    if(this.state.register_password != this.state.register_password_confirmation){
      alert('Passwords do not match.','error')
      return;
    }
    if(!this.state.register_agree){
      alert('Please, agree with out terms and conditions','error');
      return;
    }
    this.setState({registering: true})
    Ajax().post(Urls.register, {
      phone_number: this.state.register_phone_number,
      password: this.state.register_password,
      password_confirmation: this.state.register_password_confirmation,
      identity_number: this.state.register_citizenship_no
    }).then(this.verify)
  }

  verify(response) {
    this.setState({registering: false})
    if (response.status === 201) {
      alert('Registration was successful. We have send sms in your mobile number. Please verify now');
      this.setState({login: " active log-in "});
    } else{
     // alert('Something went wrong. Please try again.','error')
    }
  }

  showLogin() {
    this.state.login.includes('log-in')
      ? this.setState({
        login: this.state.login.replace(' log-in ', ' ')
      })
      : this.setState({
        login: this.state.login + ' log-in '
      })
  }

  verifyCode(event) {
    event.preventDefault();
    this.setState({verifying: true, register_password: '', register_email: '', register_password_confirmation: ''})
    Ajax().post(Urls.verify, {
      phone_number: this.state.register_phone_number,
      verification_code: this.state.verification_code
    }).then(this.verified.bind(this))
  }

  verified(response) {
    this.setState({verifying:false})
    if (response.status === 201) {
      this.setState({verified: true, login: ''});
      return;
    }
    alert('Verification process failed. Please check your verification code.','error')
  }

  validEmail(value) {
    return this.state[value].match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
  }

  allowLogin() {
    return this.state.phone_number.length == 10 && this.state.password;
  }

  allowSignUp() {
    let status = this.state.register_agree == 'on' && this.validEmail('register_email') && this.state.register_phone_number.length == 10 && this.state.register_password.length > 8 && this.state.register_password === this.state.register_password_confirmation;
    return status;
  }

  requestForNewPassword(){
    if(!this.state.phone_number){
      alert('Please input your phone number','error');
      return;
    }
    if(!this.state.phone_number > 9700000000){
      alert('Please check your phone number. It should be of 10 digits','error');
      return;
    }
    this.setState({password_requested:true});
    Ajax().post(Urls.forgot_password,{phone_number: this.state.phone_number}).then((response)=>{
        if(response.status == 202)
        {
          alert('We have just send a new password in your mobile. Please check your mobile.');
          return;
        }
        if(response.response && response.response.data && response.response.data.message && !response.response.data.errors){
          alert(response.response.data.message,'error')
          return;
        }
        return;
        alert('we are currently experience technical difficulties. Please try again later','error');
    })
  }
  throwaway(){

  }
  newPassword(answer){

  }

  setError(param) {
    if (param == 'log-in' && !this.state.displayLoginError) {
      this.setState({displayLoginError: true});
    } else if (param == 'sign-up' && !this.state.displaySignUpError) {
      this.setState({displaySignUpError: true})
    } else if (param == 'verify' && !this.state.displayVerifyError) {
      this.setState({displayVerifyError: true})
    }
  }

  render() {
    return (
      <div className="additional">
        <div className="overlay"></div>
        <div className="site-branding">
          <a href="#" className="custom-logo-link" rel="home">
            <h1>kyc nepal</h1>
          </a>
        </div>
        <div className={"container-page " + this.state.login}>
          <div className="box"></div>
          <div className="container-forms">
            <div className="container-info">
              <div className="info-item">
                <div className="table">
                  <div className="table-cell">
                    <p>
                      Have an account?
                    </p>
                    <div className="kyc-btn" onClick={this.showLogin}>
                      Log in
                    </div>
                  </div>
                </div>
              </div>
              <div className="info-item">
                <div className="table">
                  <div className="table-cell">
                    <p>
                      Don't have an account?
                    </p>
                    <div onClick={this.showLogin} className="kyc-btn sign-up-form">
                      Sign up
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="container-form">
              <div className="form-item log-in login">
                <div className="table">
                  <div className={this.state.displayLoginError
                    ? 'table-cell login-error'
                    : 'table-cell'}>
                    <form method="post">
  
                    <h4 className="login_fields__user header-login">
                      <i className="fa fa-address-card-o" aria-hidden="true"></i>
                      {this.state.verified
                        ? 'Account was successfully activated. Please, Login now'
                        : 'Please Enter Your Information'}
                    </h4>
                    <div className={(1 < this.state.phone_number.length && this.state.phone_number.length < 10)
                      ? " login_fields__user input-field alert-msg"
                      : "login_fields__user input-field"}>
                      <div className="icon">
                        <img src="assets/images/user_icon_copy.png"/>
                      </div>
                      <input onChange={this.change.bind(this, 'phone_number')} value={this.state.phone_number} type="tel" placeholder="Phone Number"/>
                      <div className="validation">
                        {!this.state.phone_number.length
                          ? "Phone Number field is required"
                          : ""}
                        {this.state.phone_number.length < 10 && this.state.phone_number.length > 1
                          ? "A valid phone number is required"
                          : ""}
                      </div>
                    </div>
                    <div className="login_fields__user input-field">
                      <div className="icon">
                        <img src="/assets/images/lock_icon_copy.png"/>
                      </div>
                      <input onChange={this.change.bind(this, 'password')} value={this.state.password} type="password" placeholder="Password"/>
                      <div className="forgot login_fields__user">
                        <a href='#' onClick={this.requestForNewPassword.bind(this)}>Forgot?</a>
                      </div>
                      <div className="validation">
                        {this.state.password.length == 0
                          ? "Password field is required"
                          : ""}
                      </div>
                    </div>
                    <div className="field login_fields__user">
                      <input type="checkbox" checked={this.state.remember_me == 'on'} id="checkbox" onChange={this.change.bind(this, 'remember_me')}/>
                      <label htmlFor="checkbox">
                        <span className="ui"></span>Keep me signed in</label>
                    </div>
                    <button type="submit" className="kyc-btn login-btn login_fields__user " onClick={ this.login.bind(this)}>
                    {this.state.logging ? <i className="fa fa-spin fa-spinner"></i> : 'Log In'}
                    </button>
                    <div className="visible-xs">
                      <p>
                        Don't have an account?
                      </p>
                      <div onClick={this.showLogin} className="kyc-btn mobile-signup sign-up-form">
                        Sign up
                      </div>
                    </div>
                    </form>
                  </div>
                </div>
              </div>
              <div className="form-item sign-up">
                <div className="table">
                  <div className={this.state.displaySignUpError
                    ? 'table-cell signup-error'
                    : 'table-cell'}>
                    <form method="post">
                    <h4 className="header-signup signup_fields__user">
                      <i className="ace-icon fa fa-users"></i>New User Registration</h4>
                    <div className="signup_fields__user input-field">
                      <div className="icon">
                        <img src="assets/images/user_icon_copy.png"/>
                      </div>
                      <input onChange={this.change.bind(this, 'register_citizenship_no')} value={this.state.register_citizenship_no} placeholder="Citizenship Number"/>

                      <div className="validation">
                        {this.state.register_citizenship_no ? <img src="assets/images/tick.png"/> : "Citizenship is required"}
                      </div>
                    </div>
                    <div className="signup_fields__user input-field">
                      <div className="icon">
                        <i className="ace-icon fa fa-mobile"></i>
                      </div>
                      <input onChange={this.change.bind(this, 'register_phone_number')} value={this.state.register_phone_number} type="tel" placeholder="Mobile Number"/>

                      <div className="validation">
                        {this.state.register_phone_number && this.state.register_phone_number.length == 10
                          ? <img src="assets/images/tick.png"/>
                        : "Mobile Number is required"}
                      </div>
                    </div>
                    <div className="signup_fields__user input-field">
                      <div className="icon">
                        <img src="assets/images/lock_icon_copy.png"/>
                      </div>
                      <input onChange={this.change.bind(this, 'register_password')} value={this.state.register_password} type="password" placeholder="Password"/>
                      <div className="validation">
                        {this.state.register_password.length > 8
                          ? <img src="assets/images/tick.png"/>
                        : "Password is too short"}
                      </div>

                    </div>
                    <div className={(this.state.register_password_confirmation === this.state.register_password && this.state.register_password && this.state.register_password_confirmation.length)
                      ? "signup_fields__user input-field"
                      : "signup_fields__user input-field alert-msg"}>
                      <div className="icon">
                        <i className="fa fa-retweet"></i>
                      </div>
                      <input onChange={this.change.bind(this, 'register_password_confirmation')} value={this.state.register_password_confirmation} type="password" placeholder="Repeat password"/>
                      <div className="validation">
                        {this.state.register_password_confirmation === this.state.register_password && this.state.register_password && this.state.register_password_confirmation.length
                          ? <img src="assets/images/tick.png"/>
                        : "Password not matched"}
                      </div>

                    </div>
                    <div className="field signup_fields__user">
                      <input type="checkbox" id="checkbox1" checked={this.state.register_agree == 'on'} onChange={this.change.bind(this, 'register_agree')}/>
                      <label htmlFor="checkbox1">
                        <span className="ui"></span>I accept the <a href='http://nepalcoin.com/terms-and-condition/'>User Agreement</a>
                      </label>

                    
                    </div>

                    <button type="submit" onClick={ this.register.bind(this) } className='kyc-btn signup-btn signup_fields__user'>
                      {this.state.registering ? <i className="fa fa-spin fa-spinner"></i> : 'Sign Up'}
                    </button>
                    <div className="visible-xs">
                      <p>
                        If you have an account.
                      </p>
                      <div onClick={this.showLogin} className="kyc-btn mobile-signup sign-up-form">
                        Login
                      </div>
                    </div>
                    </form>
                    <form method="post">

                    <h4 className="verify_fields__user header-login">
                      <i className="fa fa-address-card-o" aria-hidden="true"></i>
                      Enter the 4 digit code sent to your phone:</h4>
                    <div className="verify_fields__user input-field">
                      <div className="icon">
                        <img src="assets/images/user_icon_copy.png"/>
                      </div>
                      <input placeholder="Phone Number" disabled="disabled" value={this.state.register_phone_number} type="tel"/>
                      <div className="validation">
                        {this.state.phone_number
                          ? <img src="assets/images/tick.png"/>
                          : "Phone Number is require for verification"}
                      </div>
                    </div>
                    <div className="verify_fields__user input-field">
                      <div className="icon">
                        <img src="assets/images/lock_icon_copy.png"/>
                      </div>
                      <input placeholder="verification Code" onChange={this.change.bind(this, 'verification_code')} value={this.state.verification_code} type="password"/>
                      <div className="validation">
                        {this.state.verification_code
                          ? <img src="assets/images/tick.png"/>
                          : "Please input the verification code"}
                      </div>
                    </div>
                    <button className="kyc-btn verify-btn verify_fields__user" onClick={this.state.register_phone_number && this.state.verification_code.length == 4
                      ? this.verifyCode.bind(this)
                      : this.setError.bind(this, 'verify')}>
                      {this.state.verifying ? <i className="fa fa-spin fa-spinner"></i> : 'Verify'}
                    </button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    );
  }
}
