import {injectReducer} from '../../../store/reducers'
export default (store) => ({
    path: 'login',
    getComponent (nextState, cb) {
        require.ensure([], (require) => {
            const container = require('./container').default;
            // const reducer = require('./module').default;
            // injectReducer(store, {key: 'template', reducer});
            cb(null, container);
        }, 'login');
    }
})
