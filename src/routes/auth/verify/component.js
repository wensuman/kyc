import React from 'react';
import {IndexLink, Link} from 'react-router'
import Recaptcha from 'react-recaptcha'
import Ajax from '../../../modules/request'
import {Urls} from '../../../config/app';

export class Register extends React.Component {
  constructor(props) {
    super(props);
    this.verify = this.verify.bind(this)
    this.state = {phone_number: this.props.location.query.phone_number,verification_code:''}
    this.verified = this.verified.bind(this)
  }

  change(name,event){
    this.setState({[name]:event.target.value})
  }

  verify(){
    Ajax().post(Urls.verify,this.state).then(this.verified)
  }

  verified(response){
    if(response.status == 201){
      this.props.router.push('/login?verification=success')
    }
  }

  render() {
    return (
      <div id="signup-box" className="signup-box visible widget-box no-border">
        <div className="widget-body">
          <div className="widget-main">
            <h4 className="header green lighter bigger">
              <i className="ace-icon fa fa-users blue"></i>
              New User Registration
            </h4>

            <div className="space-6"></div>
            <p>
              Enter your details to begin:
            </p>

            <form>
              <fieldset>
                <label className="block clearfix">
                  <span className="block input-icon input-icon-right">
                    <input disabled value={this.state.phone_number} type="text" className="form-control" placeholder="Phone Number"/>
                    <i className="ace-icon fa fa-envelope"></i>
                  </span>
                </label>

                <label className="block clearfix">
                  <span className="block input-icon input-icon-right">
                    <input onChange={this.change.bind(this,'verification_code')} type="number" className="form-control" placeholder="Verification Code"/>
                    <i className="ace-icon fa fa-user"></i>
                  </span>
                </label>

                <div className="space-24"></div>

                <div className="clearfix">
                  <button onClick={this.verify.bind(this)} type="button" className="width-65 pull-right btn btn-sm btn-success">
                    <span className="bigger-110">Verify</span>
                    <i className="ace-icon fa fa-arrow-right icon-on-right"></i>
                  </button>
                </div>
              </fieldset>
            </form>
          </div>

          <div className="toolbar center">
            <Link to='/login' className="back-to-login-link">    <i className="ace-icon fa fa-arrow-left"></i>
                Back to login </Link>
          </div>
        </div>
      </div>
    )
  }
}

export default Register;
