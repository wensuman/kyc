import React from 'react';
import {IndexLink, Link} from 'react-router'
import Recaptcha from 'react-recaptcha'
import Ajax from '../../../modules/request'
import {Urls} from '../../../config/app';

export class Register extends React.Component {
  constructor(props) {
    super(props);
    this.verify = this.verify.bind(this)
  }

  change(name,event){
    this.setState({[name]:event.target.value})
  }

  register(){
    Ajax().post(Urls.register,this.state).then(this.verify)
  }

  verify(response){
    if(response.status == 201 ){
      this.props.router.push('/verify?phone_number='+this.state.phone_number)
    }
  }

  render() {
    return (
      <div id="signup-box" className="signup-box visible widget-box no-border">
        <div className="widget-body">
          <div className="widget-main">
            <h4 className="header green lighter bigger">
              <i className="ace-icon fa fa-users blue"></i>
              New User Registration
            </h4>

            <div className="space-6"></div>
            <p>
              Enter your details to begin:
            </p>

            <form>
              <fieldset>
                <label className="block clearfix">
                  <span className="block input-icon input-icon-right">
                    <input onChange={this.change.bind(this,'phone_number')} value={this.state.phone_number} type="text" className="form-control" placeholder="Phone Number"/>
                    <i className="ace-icon fa fa-envelope"></i>
                  </span>
                </label>

                <label className="block clearfix">
                  <span className="block input-icon input-icon-right">
                    <input onChange={this.change.bind(this,'email')} value={this.state.email} type="email" className="form-control" placeholder="Email (Optional)"/>
                    <i className="ace-icon fa fa-user"></i>
                  </span>
                </label>

                <label className="block clearfix">
                  <span className="block input-icon input-icon-right">
                    <input onChange={this.change.bind(this,'password')} type="password" value={this.state.password} className="form-control" placeholder="Password"/>
                    <i className="ace-icon fa fa-lock"></i>
                  </span>
                </label>

                <label className="block clearfix">
                  <span className="block input-icon input-icon-right">
                    <input onChange={this.change.bind(this,'password_confirmation')} value={this.state.password_confirmation} type="password" className="form-control" placeholder="Repeat password"/>
                    <i className="ace-icon fa fa-retweet"></i>
                  </span>
                </label>

                <label className="block">
                  <input type="checkbox" className="ace"/>
                  <span className="lbl">
                    I accept the
                    <a href="#">User Agreement</a>
                  </span>
                </label>

                <div className="space-24"></div>

                <div className="clearfix">
                  <button type="reset" className="width-30 pull-left btn btn-sm">
                    <i className="ace-icon fa fa-refresh"></i>
                    <span className="bigger-110">Reset</span>
                  </button>

                  <button onClick={this.register.bind(this)} type="button" className="width-65 pull-right btn btn-sm btn-success">
                    <span className="bigger-110">Register</span>

                    <i className="ace-icon fa fa-arrow-right icon-on-right"></i>
                  </button>
                </div>
              </fieldset>
            </form>
          </div>

          <div className="toolbar center">
            <Link to='/login' className="back-to-login-link">  <i className="ace-icon fa fa-arrow-left"></i>
                Back to login </Link>
          </div>
        </div>
      </div>
    )
  }
}

export default Register;
