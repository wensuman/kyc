// We only need to import the modules necessary for initial render
import CoreLayout from '../layouts/corelayout'
import ErrorLayout from '../layouts/errorlayout'
import DashboardRoute from './user/dashboard'
import TemplateRoute from './user/template'
import TemplateViewRoute from './user/template-view'
import LogRoute from './user/logs'
import ProfileRoute from './user/profile'
import SubmissionRoute from './user/submissions'
import AuthLayout from '../layouts/authlayout'
import LoginRoute from './auth/login'
import HistoryRoute from './user/history'
import HistoryTemplate from './user/historyForm'
import TicketRoute from './user/ticket'
import NotFoundRoute from './notfound'

export const createRoutes = (store) => ({
  childRoutes : [
    {
      component: AuthLayout,
      childRoutes: [
          LoginRoute(store),
      ]
    },
    {
      component: CoreLayout,
      childRoutes:[
        DashboardRoute(store),
        TemplateRoute(store),
        TemplateViewRoute(store),
        LogRoute(store),
        ProfileRoute(store),
        SubmissionRoute(store),
        HistoryRoute(store),
        HistoryTemplate(store),
        TicketRoute(store)
      ]
    },
    {
      component: ErrorLayout,
      childRoutes:[
        NotFoundRoute(store)
      ]
    }
  ]
})

/*  Note: childRoutes can be chunked or otherwise loaded programmatically
    using getChildRoutes with the following signature:

    getChildRoutes (location, cb) {
      require.ensure([], (require) => {
        cb(null, [
          // Remove imports!
          require('./Counter').default(store)
        ])
      })
    }

    However, this is not necessary for code-splitting! It simply provides
    an API for async route definitions. Your code splitting should occur
    inside the route `getComponent` function, since it is only invoked
    when the route exists and matches.
*/

export default createRoutes
