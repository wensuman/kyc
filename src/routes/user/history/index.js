import {injectReducer} from '../../../store/reducers'
export default (store) => ({
    path: 'history',
    getComponent (nextState, cb) {
        require.ensure([], (require) => {
            const container = require('./container').default;
            const reducer = require('./module').default;
            injectReducer(store, {key: 'history', reducer});
            cb(null, container);
        }, 'history');
    }
})
