import React from 'react';
import Ajax from '../../../modules/request';
import {Urls} from '../../../config/app';
import Axios from 'axios'
import Loader from '../../../components/loader';
import $ from 'jquery';


export class History extends React.Component {
    constructor(props) {
        super(props);
        this.state = {history: false};
        this.loadLogs = this.loadLogs.bind(this)
        this.loadLogs()
        this.componentDidMount = this.componentDidMount.bind(this)
    }
    componentDidMount(){
       setTimeout(function(){
            $('.ps-scrollbar-y-rail').css('top','0px');
        $('.ps-scrollbar-y').css('top','0px');
    },200)
    }

    loadLogs(){
        Ajax().get(Urls.form.history).then((response)=>{this.setState({history: response.data.data})});
    }

    destroySubmission(submissionId){
      confirm('You are trying to remove the access of bank to this submissions.', this.removeSubmission.bind(this, submissionId))
    }

    removeSubmission(id){
      this.setState({['delete_text_'+id]: 'Deleting'})
      Ajax().delete(Urls.form.delete+id).then((response)=>{
          this.loadLogs()
          this.setState({['delete_text'+id]:'Deleted'})
      })

    }

    attribute(history,index){
        return (<tr key={index}>
          <td>{history.id}</td>
          <td>{history.bank_name}</td>
          <td>{history.created_at}</td>
          <td  className={history.verification}>{history && history.verification ? (history.verification == 'pending' ?'Verification on Process' : history.verification)  : 'Verification on Process'}</td>
          <td>
            <button className="btn btn-rose btn-fill" onClick={this.props.router.push.bind(this,'/history/'+history.id)}>{history.verification == 'rejected' ? 'Edit' : 'View'}</button>
          </td>
        </tr>)
    }
            // {-- <button className="btn btn-danger btn-fill" onClick={this.removeSubmission.bind(this,history.id)}>{this.state['delete_text_'+history.id] || 'Delete'}</button> --}
  render() {
  if(!this.state.history){
    return  <Loader />;
  }
  if(this.state.history.length == 0){
    return (
      <div className="transition-item animated fadeIn" >
        <div className="col-sm-12">
          <div className="card card-stats">
            <div className="card-header card-header-icon" data-background-color="rose">
                  <i className="material-icons">history</i>
            </div>
            <div className="card-content">
              <h3 className="card-title">Your submission List</h3>
              <div>
                <div className="alert alert-info">
                    <span>You do not have any submissions made to the bank.</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  const html = this.state.history.map(this.attribute.bind(this))
  let htmlFull = (
    <table id="simple-table" className="table">
    <thead>
      <tr>
        <th>Submission Id</th>
        <th>Bank Name</th>
        <th>Submission Date</th>
        <th>Status</th>
        <th>Actions</th>
      </tr>
    </thead>

    <tbody>
    {html}
    </tbody>
    </table>
    )
  return (
  <div className="transition-item animated fadeIn" >
    <div className="col-sm-12">
      <div className="card card-stats">
        <div className="card-header card-header-icon" data-background-color="rose">
              <i className="material-icons">history</i>
        </div>
        <div className="card-content">
          <h3 className="card-tite">Your submission List</h3>
          <div className="table-responsive">
            {htmlFull}
          </div>
        </div>
      </div>
    </div>
  </div>
    );
  }
}


export default History
