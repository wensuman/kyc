import {connect} from 'react-redux'

import HistoryTemplate from './component'

const mapDispatchToProps = {
};

const mapStateToProps = function (state) {
    return state
}


export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(HistoryTemplate);
