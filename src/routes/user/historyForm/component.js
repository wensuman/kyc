import React from 'react';
import Ajax from '../../../modules/request';
import {Urls} from '../../../config/app';
import Axios from 'axios'
import Loader from '../../../components/loader'
import Form from '../../../components/form';
import $ from 'jquery';


export class HistoryTemplate extends React.Component {
    constructor(props) {
        super(props);
        this.state = {loaded:false}
        this.componentDidMount = this.componentDidMount.bind(this)
    }

    componentDidMount(){
       setTimeout(function(){
            $('.ps-scrollbar-y-rail').css('top','0px');
        $('.ps-scrollbar-y').css('top','0px');
    },200)
        Ajax().get(Urls.form.history+this.props.router.params.id).then((response)=>{this.setState({history: response.data.data,loaded:true})})
    }
    success(){
      this.props.router.push('/history');
    }

    render() {
      if(!this.state.loaded){
        return <Loader/>;
      }
      return (
        <div className="transition-item history-form animated fadeIn">
            <Form type="history" success={this.success.bind(this)} postUrl={Urls.form.submission + this.props.router.params.id} stats={this.state.history}  />
        </div>
      );
    }
}


export default HistoryTemplate
