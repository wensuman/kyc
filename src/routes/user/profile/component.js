import React from 'react';
import {IndexLink, Link} from 'react-router';
import Ajax from '../../../modules/request';
import {Urls} from '../../../config/app';
import '../../../styles/dropzone.css'
import './style/change-password.css';
import userPropImage from './images/marc.jpg';
import {Countries, Districts, Zones} from '../../../config/lists';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import Dropzone from 'react-dropzone';
import request from 'superagent';
import Loader from '../../../components/loader'
import userImage from './images/profil-pic_dummy.png';
import $ from 'jquery';

export class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      old_password: '',
      new_password: '',
      new_password_confirmation: '',
      changingPassword: '',
      identity_number:'',
      first_name:'',
      middle_name:'',
      last_name:'',
      phone_number:'',
      email:'',
      address:'',
      country:'Nepal',
      zone:'bagmati',
      district:'kathmandu',
      pic:'',
      loading:true
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount(){
     setTimeout(function(){
            $('.ps-scrollbar-y-rail').css('top','0px');
        $('.ps-scrollbar-y').css('top','0px');
    },200)
    Ajax().get(Urls.form.template).then((response)=>{
      let data = response.data.data;
      this.setState(data);
      this.setState({loading:false});
    })
  }

  change(name, event) {
    this.setState({
      [name]: event.value
        ? event.value
        : event.target.value
    });
  }

  changePassword(event) {
    event.preventDefault();
    let condition = this.formValid.bind(this).apply();
    if (condition)
      this.setState({'changingPassword': 'fa fa-spinner fa-spin'})
    Ajax().post(Urls.changePassword, this.state).then(this.success.bind(this)).catch(this.error.bind(this))
  }
  success(response) {
    if (response.status === 202) {
      this.setState({'changingPassword': ' fa fa-check ', 'new_password': '', 'old_password': '', 'new_password_confirmation': ''});
      alert('Password Changed Successfully', 'success')
      return;
    }
    alert('Something went wrong. Please make sure, your old password is correct.', 'error');
  }
  error() {
    this.setState({'changingPassword': ''});
  }

  formValid() {
    return this.state.old_password && this.state.new_password.length > 8 && (this.state.new_password == this.state.new_password_confirmation);
  }


  onDrop(meta, acceptedFiles, rejectedFiles) {
      const req = request.post(Urls.upload);
      req.attach('file', acceptedFiles[0]);
      this.setState({uploading: true});
      req.end((err, response) => {
        if(err && err.response && err.response.text){
          let json = JSON.parse(err.response.text)
          alert(json.errors.file[0],'error')
          this.setState({uploading: false})
        }else{
          this.setState({[meta]: response.body.data[0].name,pic: response.body.data[0].name, uploading: false, change: true});
        }
        this.updateProfile();
      });
  }
  render() {
    if(this.state.loading){
      return <div><Loader/></div>;
    }
    return (
      <div className="container-fluid profile-section animated fadeIn">
        <div className="row">
          <div className="col-md-12">
            <div className="card">
              <div className="card-header card-header-icon" data-background-color="rose">
                <i className="material-icons">lock</i>
              </div>
              <div className="card-content">
                <h5>Change Password</h5>
                <div className="logo"></div>
                    <div className="form-group label-floating is-empty">
                      <label className="control-label">Old Password</label>
                      <input className='form-control' title="Please input your old password" id="passOne" onChange={this.change.bind(this, 'old_password')} type="password" value={this.state.old_password}/>
                    </div>
                    <div className="form-group label-floating is-empty">
                      <label className="control-label">New Password</label>
                      <input title="Password must be atleast 8 character" id="passThree" onChange={this.change.bind(this, 'new_password')} type="password" className="form-control" value={this.state.new_password}/>
                    </div>
                    <div className="form-group label-floating is-empty">
                      <label className="control-label">Confirm New Password</label >
                      <input title="Type your password again" id="passTwo" onChange={this.change.bind(this, 'new_password_confirmation')} className="form-control" type="password" value={this.state.new_password_confirmation}/>
                      <div className="hide-show"></div>
                    </div>
                    <button className="btn btn-rose btn-fill" title="Pressing this button will change your password" onClick={this.changePassword.bind(this)} disabled={this.state.changingPassword == ''
                      ? ''
                      : 'disabled'}>
                      <i className={this.state.chanogingPassword}></i>
                      {this.state.changingPassword == ''
                        ? 'Change Password'
                        : ''}</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Profile;
