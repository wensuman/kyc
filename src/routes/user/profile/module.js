export const DASHBOARD = 'DASHBOARD';

export function dashboard() {
    return {
        type: DASHBOARD,
        payload: ''
    }
}

export const actions = {
}

const ACTION_HANDLERS = {
};

const initialState = {}

export default function reducer(state = initialState, action) {

    const handler = ACTION_HANDLERS[action.type]

    return handler ? handler(state, action) : state
}
