import {injectReducer} from '../../../store/reducers'
import IsLoggedIn from '../../../modules/sessions'
export default (store) => ({
    path: 'support',
    onEnter(props, replace){
        if (!IsLoggedIn()) {
            // replace('/login');
        }
    },
    getComponent (nextState, cb) {
        require.ensure([], (require) => {
            const container = require('./container').default;
            // const reducer = require('./module').default;
            // injectReducer(store, {key: 'support', reducer});
            cb(null, container);
        }, 'support');
    }
})
