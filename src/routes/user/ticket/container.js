import {connect} from 'react-redux'

import Ticket from './component'

const mapDispatchToProps = {
};

const mapStateToProps = function (state) {
    return state
}


export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(Ticket)
