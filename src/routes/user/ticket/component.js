import React from 'react';
import {IndexLink, Link} from 'react-router';
import Form from '../../../components/form';
import {Urls} from '../../../config/app';
import Ajax from '../../../modules/request';
import Select from 'react-select';
import $ from 'jquery';

export class Ticket extends React.Component {
    constructor(props) {
        super(props);
        this.state = {loaded:false,bank_id:'',question_title:'',question_detail:'',tickets:[],pagination:{links:{}}};
        this.loadTickets = this.loadTickets.bind(this)
        this.loadBanks = this.loadBanks.bind(this)
        this.componentDidMount = this.componentDidMount.bind(this)
    }

    componentDidMount(){
        setTimeout(function(){
            $('.ps-scrollbar-y-rail').css('top','0px');
        $('.ps-scrollbar-y').css('top','0px');
        },200);
        this.loadTickets();
        this.loadBanks();
    }

    loadTickets(url){
        Ajax().get(url || Urls.tickets).then((response)=>{this.setState({tickets:response.data.data,pagination: response.data.meta.pagination})})
    }

    loadBanks(){
         Ajax().get(Urls.banks).then((response)=>this.setState({
            banks:response.data.data.map(function(bank) {
            return {label: bank.profile.name, value: bank.id}
          })}))
    }

    change(name, value, event, multi=false) {
    if(value == true){
        let state = this.state[name].slice();
        state.push(event)
        this.setState({[name]: state})
    }
    if (value && value.target && (value.target.value || value.target.value === '')) {
        this.setState({[name]: value.target.value, change: true});
    } else if (value.label) {
        this.setState({[name]: value.value, change: true})
    } else {
        this.setState({
        change: true,
        [name]: value
          ? value
          : event.target.value
        });
    }
    this.setState({applicant_name: this.state.first_name + ' ' + this.state.middle_name + ' ' + this.state.last_name})
    }

    submit(){
        Ajax().post(Urls.tickets,{title:this.state.question_title,detail: this.state.question_detail, bank_id: this.state.bank_id}).then((response)=>{
            if(response.status == 201){
                alert('Your ticket has been successfully created.');
                this.setState({question_detail:'',question_title:''});
                this.loadTickets();
                return;
            }
            alert('Something went wrong. Please try again later.','error')
        })
    }

    render() {
        return (
                <div className="card animated fadeIn">
                     <div className="card-header card-header-icon" data-background-color="rose"><i className="material-icons">headset_mic</i></div>
                <div className="card-content">
                    <h4 className="card-title">Your Support Tickets</h4>
                    <form>
                        <div className="col-sm-12">
                            <div className="create-ticket-wrapper">
                                <div className="col-sm-12">
                                    <div className="col-sm-3">
                                        <label>Select a bank</label>
                                    </div>
                            <div className="col-sm-9">
                                        <Select searchable={false} clearable={false} placeholder="Select a bank" value={this.state.bank_id} style={{
                            zIndex: 3
                          }} onChange={this.change.bind(this, 'bank_id')} clearable={false} options={this.state.banks}/>
                                </div>
                            </div>
                            <div className="col-md-12">
                                <div className={this.state.question_title ? "form-group label-floating" :"form-group label-floating is-empty" }>
                                    <label className="control-label">Question Title</label>
                                    <input onChange={this.change.bind(this,'question_title')}  value={this.state.question_title} className="form-control" name="about" id="" cols="80" rows="1"/>
                                    <span className="material-input"></span><span className="material-input"></span>
                                </div>
                            </div>
                            <div className="col-md-12">
                                <div className={this.state.question_detail ? "form-group label-floating" : "form-group label-floating is-empty"}>
                                    <label className="control-label">Question Description</label>
                                    <textarea onChange={this.change.bind(this,'question_detail')} className="form-control" value={this.state.question_detail} name="about" id="" cols="80" rows="5"></textarea>
                                    <span className="material-input"></span><span className="material-input"></span>
                                </div>
                            </div>
                            <div className="col-sm-12">
                                <button onClick={this.submit.bind(this)} disabled={!this.state.question_title} title="Submitting your question" type="button" className="btn btn-primary">
                                            Submit
                                    <div className="ripple-container"></div>
                                </button>
                            </div>
                        </div>  
                        </div>
                        <div className="col-sm-12">
                            <div className="ticket-response">
                                <div className="response-wrapper">
                                    {this.state.tickets.map((map,index)=>(
                                        <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                        <div className="panel panel-default">
                                            <div className="panel-heading" role="tab" id="headingOne">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href={'#collapse'+index} aria-expanded="false" aria-controls="collapseOne" className="collapsed">
                                                    <h4 className="panel-title">
                                                        {map.ticket.title}                 
                                                        <i className="material-icons">keyboard_arrow_down</i>
                                                    </h4>
                                                </a>
                                                 {map.ticket.answer ? <span className="checked-question">
                                                       <i className="material-icons">done</i>
                                                    </span> : ''}
                                            </div>
                                            <div id={'collapse'+index} className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style={{height:'0px'}}>
                                                <div className="panel-body">
                                                <div className="col-xs-12">{map.ticket.detail}</div>
                                                {map.ticket.answer ? <div className="col-xs-12"><div className="col-xs-1">Ans:</div><span>{map.ticket.answer}</span></div> : ''} 
                                               </div>
                                            </div>
                                        </div>
                                    </div>
                                    ))}
                                </div>
                                <div className="col-xs-12">
                                    <div className='pagination-buttons pull-right'>
                                        {this.state.pagination.links.previous ? <button className="btn btn-primary"  onClick={(event)=>{event.preventDefault();this.loadTickets(this.state.pagination.links.previous)}}>Previous</button> : '' }
                                        {this.state.pagination.links.next ? <button className="btn btn-primary" onClick={(event)=>{event.preventDefault();this.loadTickets(this.state.pagination.links.next)}}>Next</button> : '' }
                                    </div>
                                </div>
                           </div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default Ticket;
  
            