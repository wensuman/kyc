import React from 'react';
import {IndexLink, Link} from 'react-router';
import Form from '../../../components/form';
import {Urls} from '../../../config/app';
import Ajax from '../../../modules/request';
import Loader from '../../../components/loader';
import $ from 'jquery';


export class Template extends React.Component {
    constructor(props) {
        super(props);
        this.componentDidMount = this.componentDidMount.bind(this)
        this.state={template:{}}
    }

    componentDidMount(){
         setTimeout(function(){
            $('.ps-scrollbar-y-rail').css('top','0px');
        $('.ps-scrollbar-y').css('top','0px');
    },200)
        Ajax().get(Urls.form.template).then((response)=>{
            if( !response.data.data){
                return;
            }
            if(response.data.data.verification == 'verified' || response.data.data.verification == 'pending'){
                this.props.router.push('/template');
            }
            this.setState({template:response.data.data})});
    }
    render() {
        return <Form type="template" onVerifyRequest={()=>{this.props.router.push('/template')}} postUrl={Urls.form.template} />;
    }
}


export default Template
