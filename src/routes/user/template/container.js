import {connect} from 'react-redux'

import Component from './component'

const mapDispatchToProps = {
};

const mapStateToProps = function (state) {
    return state
}


export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(Component)
