import React from 'react';
import './style/faq.css';
import $ from 'jquery';

export class Faq extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="transition-item animated fadeIn">
        <section className="content-block">
          <h1>
            What is KYC?</h1>
          <p>
            Know Your Customer - KYC enables banks to know/ understand their customers and their financial dealings to be able to serve them better.
          </p>
        </section>

        <section className="content-block">
          <h1>
            Why KYC?
          </h1>
          <p>
            To establish the identity of the client : This means identifying the customer and verifying his/ her identity by using reliable, independent source documents, data or information. For individuals, bank will obtain identification data to verify the identity of the customer, his address/ location and also his recent photograph. This will be done for the joint holders and mandate holders as well. For non-individuals, bank will obtain identification data to:
          </p>
          <ul>
            <li>
              verify the legal status of the legal person/ entity
            </li>
            <li>
              verify identity of the authorized signatories and
            </li>
            <li>
              verify identity of the Beneficial owners/ controllers of the account
            </li>
          </ul>
          <p>
            To ensure that sufficient information is obtained on the nature of employment/ business that the customer does / expects to undertake and the purpose of the account
          </p>

        </section>
        <section className="content-block">
          <h1>
            Are KYC requirements new?
          </h1>
          <p>
            No, KYC requirements have always been in place and Banks have been taking KYC documents in accordance with the guidelines issued by RBI from time to time. RBI has revisited the KYC guidelines in the context of recommendations made by the Financial Action Task Force (FATF) on Anti Money Laundering standards and on Combating Financing of Terrorism and enhanced the KYC standards in line with international benchmarks
          </p>
        </section>
      </div>
    );
  }
}

export default Faq
