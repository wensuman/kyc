import {injectReducer} from '../../../store/reducers'
import IsLoggedIn from '../../../modules/sessions'
export default (store) => ({
    path: 'template',
    onEnter(props, replace){
        if (!IsLoggedIn()) {
            // replace('/login');
        }
    },
    getComponent (nextState, cb) {
        require.ensure([], (require) => {
            const container = require('./container').default;
            // const reducer = require('./module').default;
            // injectReducer(store, {key: 'template/view', reducer});
            cb(null, container);
        }, 'template');
    }
})
