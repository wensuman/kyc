import React from 'react';
import {IndexLink, Link} from 'react-router';
import Form from '../../../components/form';
import {Urls} from '../../../config/app';
import Loader from '../../../components/loader';
import Ajax from '../../../modules/request';
import Attributes from '../../../components/form/attributes';
import $ from 'jquery';


export class Template extends React.Component {
    constructor(props) {
        super(props);
        this.componentDidMount = this.componentDidMount.bind(this)
        this.state={loaded:false,template:{self_employed:[],card_required_types:[],business_types:[]}};
    }

    componentDidMount(){
       setTimeout(function(){
            $('.ps-scrollbar-y-rail').css('top','0px');
        $('.ps-scrollbar-y').css('top','0px');
    },200)
        Ajax().get(Urls.form.template).then((response)=>{
          this.setState({loaded:true})
        if(!response.data.data){
          this.props.router.push('/template/edit');
          return;
        }
        if(response.data.data){
          this.setState({template:response.data.data});
        } 
      });
    }

    render() {
        if(!this.state.loaded ){
          return <Loader />;
        }
        let verificationStatus = (<div className="clearfix" style={{margin: '13px 13px 14px'}}><Link to='/template/edit' className="pull-right  btn btn-primary">Edit</Link></div>);
        if(this.state.template && this.state.template.verification == 'verified'){
          verificationStatus = (<div  className=" alert alert-success form-alert">
                                        <span>
                                            Your template has been successfully verified.</span>
                                    </div>);
        }
      
        if(this.state.template && this.state.template.verification == 'pending'){
          verificationStatus = (<div  className=" alert alert-success form-alert">
                                        <span>
                                            Your template is currently under verification process.</span>
                                    </div>);
        }
        return (
        <div className="card animated fadeIn">
        <div className="card-header card-header-icon" data-background-color="rose">
            <i className="material-icons">receipt</i>
        </div>
        <div className="card-content customer-display">
            {verificationStatus}
            <div className="dashboard-template" >
              <section className="template-part">
                <caption className="header-title">Identity Details</caption>
                <div className="row no-margin">
                  <div className="col-xs-12 no-padding">
                    <div className="template-part-detail">
                      <span className="pull-left"><b>Name of the Applicant</b></span>
                      <span className="pull-right">{this.state.template.title} {this.state.template.applicant_name}</span>
                    </div>
                    <div className="template-part-detail">
                      <span className="pull-left"><b>Currency</b></span>
                      <span className="pull-right">{this.state.template.currency ? this.state.template.currency.value: ''} {this.state.template.currency && this.state.template.currency.value == 'others' ? <span>({this.state.template.other_currency})</span> : ''}</span>
                    </div>
                    <div className="template-part-detail">
                      <span className="pull-left"><b>Father's Name</b></span>
                      <span className="pull-right">{this.state.template.fathers_name}</span>
                    </div>
                    <div className="template-part-detail">
                      <span className="pull-left"><b>Grand Father's Name</b></span>
                      <span className="pull-right">{this.state.template.grand_fathers_name}</span>
                    </div>
                    <div className="template-part-detail">
                      <span className="pull-left"><b>Husband's / Wife's Name</b></span>
                      <span className="pull-right">{this.state.template.spouse_name}</span>
                    </div>
                    <div className="template-part-detail">
                      <span className="pull-left"><b>Guardian's Name (in case of minor)</b></span>
                      <span className="pull-right">{this.state.template.guardians_name}</span>
                    </div>
                    <div className="template-part-detail">
                      <span className="pull-left"><b>Marital Status</b></span>
                      <span className="pull-right">{this.state.template.marital_status ? this.state.template.marital_status.value : ''}</span>
                    </div>
                    <div className="template-part-detail">
                      <span className="pull-left"><b>Status</b></span>
                      <span className="pull-right">{this.state.template.status ? this.state.template.status.label : ''}</span>
                    </div>
                     <div className="template-part-detail">
                      <span className="pull-left"><b>Occupation</b></span>
                      <span className="pull-right">{this.state.template.occupation ? this.state.template.occupation.value : ''}{this.state.template.occupation && this.state.template.occupation.value == 'Other' ? <span>({this.state.template.other_occupation})</span> : ''}</span>
                    </div>
                    <div className="template-part-detail">
                      <span className="pull-left"><b>Total Annual Income</b></span>
                      <span className="pull-right">{this.state.template.income}</span>
                    </div>
                    <div className="template-part-detail">
                      <span className="pull-left"><b>Employment Type( In case of self employment)</b></span>
                      <span className="pull-right">{this.state.template.self_employed ? this.state.template.self_employed.map((map)=>{return map.label}).join(', ') : ''} {this.state.template.other_employement ? <span>({this.state.template.other_employement})</span>:'' }</span>
                    </div>
                    <div className="template-part-detail">
                      <span className="pull-left"><b>Citizenship Number</b></span>
                      <span className="pull-right">{this.state.template.citizenship}</span>
                    </div>
                    <div className="template-part-detail">
                      <span className="pull-left"><b>Nationality</b></span>
                      <span className="pull-right">{this.state.template.nationality}</span>
                    </div>
                    <div className="template-part-detail">
                      <span className="pull-left"><b>Passport Number</b></span>
                      <span className="pull-right">{this.state.template.passport_number}</span>
                    </div>
                    <div className="template-part-detail">
                      <span className="pull-left"><b>Driving Lisence Number</b></span>
                      <span className="pull-right">{this.state.template.driving_liscence_number}</span>
                    </div>
                    <div className="template-part-detail">
                      <span className="pull-left"><b>Religion</b></span>
                      <span className="pull-right">{this.state.template.religion ? this.state.template.religion.label : ''} {(this.state.template.other_teligion && this.state.template.religion && this.state.template.religion.value &&  this.state.template.religion.value.indeOf('ther') > -1) ? <span>({this.state.template.other_teligion})</span>:''}</span>
                    </div>  
                    <div className="template-part-detail">
                      <span className="pull-left"><b>Education Qualification</b></span>
                      <span className="pull-right">{this.state.template.education ? this.state.template.education.value : ''}{(this.state.template.other_qualification && this.state.template.education && this.state.template.education.value &&  this.state.template.education.value.indexOf('ther') > -1) ? <span>({this.state.template.other_qualification})</span>:''}</span>
                    </div>       
                    <div className="template-part-detail">
                      <span className="pull-left"><b>Are you availing Debit/Credit Card facility</b></span>
                      <span className="pull-right">{this.state.template.card_facility}</span>
                    </div> 
                    <div className="template-part-detail">
                      <span className="pull-left"><b>If Yes, select the type of card</b></span>
                      <span className="pull-right">{this.state.template.card_required_types ? this.state.template.card_required_types.map((map)=>{return map.label}).join(', ') : ''}</span>
                    </div>
                    <div className="template-part-detail">
                      <span className="pull-left"><b>Name of card Issuance Bank/s</b></span>
                      <span className="pull-right">suman you need to work on it</span>
                    </div>                 
                 </div>
                </div>
              </section>

            </div>







                        <div className="dashboard-template" >
              <section className="template-part">
                <caption className="header-title">NRN Details (In case of person being NRN)</caption>
                <div className="row no-margin">
                  <div className="col-xs-12 no-padding">
                    <div className="template-part-detail">
                      <span className="pull-left"><b>Is account of a non resident or a foriegn national?</b></span>
                      <span className="pull-right">{this.state.template.isNrn}</span>
                    </div>

                    <div className="template-part-detail">
                      <span className="pull-left"><b>Date of becoming nrn</b></span>
                      <span className="pull-right">{this.state.template.nrn_date_of_becoming_nrn}</span>
                    </div>

                    <div className="template-part-detail">
                      <span className="pull-left"><b>Country</b></span>
                      <span className="pull-right">{this.state.template.nrn_country}</span>
                    </div>

                    <div className="template-part-detail">
                      <span className="pull-left"><b>Visa No</b></span>
                      <span className="pull-right">{this.state.template.nrn_visa_no}</span>
                    </div>

                    <div className="template-part-detail">
                      <span className="pull-left"><b>Date of issue</b></span>
                      <span className="pull-right">{this.state.template.nrn_date_of_issue}</span>
                    </div>

                    <div className="template-part-detail">
                      <span className="pull-left"><b>Validity</b></span>
                      <span className="pull-right">{this.state.template.nrn_validity}</span>
                    </div>
                
                 </div>
                </div>
              </section>

            </div>


             <div className="dashboard-template" >
              <section className="template-part">
                <caption className="header-title">Contact Person In Nepal</caption>
                <div className="row no-margin">
                  <div className="col-xs-12 no-padding">
                    <div className="template-part-detail">
                      <span className="pull-left"><b>Name</b></span>
                      <span className="pull-right">{this.state.template.contact_person_name}</span>
                    </div>

                    <div className="template-part-detail">
                      <span className="pull-left"><b>Zone</b></span>
                      <span className="pull-right">{this.state.template.contact_person_name}</span>
                    </div>

                    <div className="template-part-detail">
                      <span className="pull-left"><b>Municipality/VDC</b></span>
                      <span className="pull-right">{this.state.template.contact_person_municipality}</span>
                    </div>

                    <div className="template-part-detail">
                      <span className="pull-left"><b>Phone Number</b></span>
                      <span className="pull-right">{this.state.template.contact_person_phone_no}</span>
                    </div>

                    <div className="template-part-detail">
                      <span className="pull-left"><b>Fax No.</b></span>
                      <span className="pull-right">{this.state.template.fax_no}</span>
                    </div>

                    <div className="template-part-detail">
                      <span className="pull-left"><b>Relationship To Me</b></span>
                      <span className="pull-right">{this.state.template.contact_person_relationship_to_me}</span>
                    </div>

                    <div className="template-part-detail">
                      <span className="pull-left"><b>Address</b></span>
                      <span className="pull-right">{this.state.template.contact_person_address}</span>
                    </div>

                    <div className="template-part-detail">
                      <span className="pull-left"><b>District</b></span>
                      <span className="pull-right">{this.state.template.contact_person_district}</span>
                    </div>

                    <div className="template-part-detail">
                      <span className="pull-left"><b>Ward No</b></span>
                      <span className="pull-right">{this.state.template.contact_person_ward_no}</span>
                    </div>

                    <div className="template-part-detail">
                      <span className="pull-left"><b>Mobile Number</b></span>
                      <span className="pull-right">{this.state.template.contact_person_mobile_number}</span>
                    </div>

                    <div className="template-part-detail">
                      <span className="pull-left"><b>Email</b></span>
                      <span className="pull-right">{this.state.template.contact_person_email}</span>
                    </div>

                
                 </div>
                </div>
              </section>

            </div>




            <div className="dashboard-template" >
              <section className="template-part">
                <caption className="header-title">Nature of business</caption>
                <div className="row no-margin">
                  <div className="col-xs-12 no-padding">
                    <div className="template-part-detail">
                      <span className="pull-left"><b>Nature of Business</b></span>
                      <span className="pull-right"> {this.state.template.business_types ? this.state.template.business_types.map((map)=>{return map.label}).join(', ') : ''}</span>
                    </div>

                    <div className="template-part-detail">
                      <span className="pull-left"><b>Total Annual turnover</b></span>
                      <span className="pull-right">{this.state.template.annual_turn_over}</span>
                    </div>

                    <div className="template-part-detail">
                      <span className="pull-left"><b>Whether Income Tax Assessee?</b></span>
                      <span className="pull-right">{this.state.template.income_tax_assess}</span>
                    </div>

                    <div className="template-part-detail">
                      <span className="pull-left"><b>PAN No</b></span>
                      <span className="pull-right">{this.state.template.pan_no}</span>
                    </div>

                    <div className="template-part-detail">
                      <span className="pull-left"><b>VAT No</b></span>
                      <span className="pull-right">{this.state.template.vat_no}</span>
                    </div>

                    <div className="template-part-detail">
                      <span className="pull-left"><b>Registered In</b></span>
                      <span className="pull-right">{this.state.template.registered_on}</span>
                    </div>

                    <div className="template-part-detail">
                      <span className="pull-left"><b>Registered In</b></span>
                      <span className="pull-right">{this.state.template.registered_no}</span>
                    </div>

              
                 </div>
                </div>
              </section>

            </div>





                        <div className="dashboard-template" >
              <section className="template-part bank-details-template">
                <div className="row no-margin">
                 {Attributes.num_of_banks.map((map)=>{
                              if(this.state.template['other_bank_detail_'+map+'_name']){
                                return (
                 <div>
                    <div className="col-xs-6 no-padding">
                    <div className="template-part-detail">
                      <span className="pull-left"><b>Name of the Bank &amp; Branch</b></span><br />
                      <span className="pull-left"> {this.state.template['other_bank_detail_'+map+'_name']}</span>
                    </div>
                 </div>
                 <div className="col-xs-6 no-padding">
                    <div className="template-part-detail">
                      <span className="pull-left"><b>Facilities/services being availed</b></span><br />
                      {Attributes.account_type_of_banks.map((type)=>(
                      <span className="pull-left">
                                      {this.state.template['other_bank_detail_'+map+'_details'].filter((map)=>{return map.label==type}).map((map)=>{return map.label}).join(', ') }
                                   </span>
                                    ))}
                    </div>
                 </div>
                 </div>
              )}})}
              </div>
              </section>
            </div>
            <div className="dashboard-template" >
              <section className="template-part">
                <caption className="header-title">Permanent Address</caption>
                <div className="row no-margin">
                  <div className="col-xs-12 no-padding">
                   {Attributes.address_main.map((map)=>(
                    <div className="template-part-detail">
                      <span className="pull-left"><b>{map.name}</b></span>
                      <span className="pull-right"> {this.state.template['permanent_'+map.state]}</span>
                    </div>
                             ))}
                   {Attributes.address_inputs.map((map)=>(
                    <div className="template-part-detail">
                      <span className="pull-left"><b>{map.name}</b></span>
                      <span className="pull-right"> {this.state.template['permanent_'+map.state]}</span>
                    </div>
                             ))}
                 </div>

                </div>
              </section>

            </div>

            <div className="dashboard-template" >
              <section className="template-part">
                <caption className="header-title">Temporary Address</caption>
                <div className="row no-margin">
                  <div className="col-xs-12 no-padding">
                   {Attributes.address_main.map((map)=>(
                    <div className="template-part-detail">
                      <span className="pull-left"><b>{map.name}</b></span>
                      <span className="pull-right"> {this.state.template['permanent_'+map.state]}</span>
                    </div>
                             ))}
                   {Attributes.address_inputs.map((map)=>(
                    <div className="template-part-detail">
                      <span className="pull-left"><b>{map.name}</b></span>
                      <span className="pull-right"> {this.state.template['temporary_'+map.state]}</span>
                    </div>
                             ))}
                 </div>
                </div>
              </section>

            </div>

            <div className="dashboard-template" >
              <section className="template-part">
                <caption className="header-title">Correspondence Address</caption>
                <div className="row no-margin">
                  <div className="col-xs-12 no-padding">
                   {Attributes.address_main.map((map)=>(
                    <div className="template-part-detail">
                      <span className="pull-left"><b>{map.name}</b></span>
                      <span className="pull-right"> {this.state.template['permanent_'+map.state]}</span>
                    </div>
                             ))}
                   {Attributes.address_inputs.map((map)=>(
                    <div className="template-part-detail">
                      <span className="pull-left"><b>{map.name}</b></span>
                      <span className="pull-right"> {this.state.template['correspondence_'+map.state]}</span>
                    </div>
                             ))}
                 </div>
                </div>
              </section>

            </div>



            <div className="dashboard-template" >
              <section className="template-part bank-details-template">
                <caption className="header-title">Additional Details of Family Members</caption>
                <div className="row no-margin">
                 {[0,1,2,3,4,5,6,7,8,9].map((map,index)=> (
 <div key={index} style={{display: this.state.template['relation_'+map+'_name'] ?'':'none'}}>
                    <div className="col-xs-6 no-padding">
                    <div className="template-part-detail">
                      <span className="pull-left"><b>Relations</b></span><br />
                      <span className="pull-left"> {this.state.template['relation_'+map+'_type'] ? this.state.template['relation_'+map+'_type'].value : '' }</span>
                    </div>
                 </div>
                  <div className="col-xs-6 no-padding">
                    <div className="template-part-detail">
                      <span className="pull-left"><b>Relations details</b></span><br />
                           <span className="pull-left">
                                    <b>Name: </b> {this.state.template['relation_'+map+'_name']}
                                   </span><span className="pull-left">
                                    <b>Citizenship No:</b>   {this.state.template['relation_'+map+'_citizenship_no']}
                                   </span><span className="pull-left">
                                    <b>Issued By:</b>  {this.state.template['relation_'+map+'_issued_by']}
                                   </span><span className="pull-left">
                                    <b>Issued On:</b>   {this.state.template['relation_'+map+'_issued_on']}
                                   </span>
                     </div>
                   </div>
                  </div>
                                )
                              )}
                 
              </div>
              </section>
            </div>

                  {this.state.template.photo_person ? <div className="full-width-image-wrap table-responsive info-review-table">
                                <h2 className="align-center">person</h2>
                      <img style={{borderWidth: '2px', borderColor: 'rgb(102, 102, 102)', borderStyle: 'dashed', borderRadius: '5px', maxWidth: '100%', width:'auto', height:'auto'}} src={Urls.site+this.state.template.photo_person} alt="person"/>
                      </div> : ''}
                    {this.state.template.photo_bill ? <div className="full-width-image-wrap table-responsive info-review-table">
                          <h2 className="align-center">bill</h2>
                          <img style={{borderWidth: '2px', borderColor: 'rgb(102, 102, 102)', borderStyle: 'dashed', borderRadius: '5px', maxWidth: '100%', width:'auto', height:'auto'}} src={Urls.site+this.state.template.photo_bill} alt="bill"/>
                      </div> : ''}
                    {this.state.template.photo_liscence ? <div className="full-width-image-wrap table-responsive info-review-table">
                          <h2 className="align-center">liscence</h2>
                          <img style={{borderWidth: '2px', borderColor: 'rgb(102, 102, 102)', borderStyle: 'dashed', borderRadius: '5px', maxWidth: '100%', width:'auto', height:'auto'}} src={Urls.site+this.state.template.photo_liscence} alt="liscence"/>
                      </div> : ''}
                    {this.state.template.photo_citizenship ? <div className="full-width-image-wrap table-responsive info-review-table">
                          <h2 className="align-center">citizenship</h2>
                          <img style={{borderWidth: '2px', borderColor: 'rgb(102, 102, 102)', borderStyle: 'dashed', borderRadius: '5px', maxWidth: '100%', width:'auto', height:'auto'}} src={Urls.site+this.state.template.photo_citizenship}  alt="citizenship"/>
                      </div>: '' }
                    {this.state.template.photo_voters_identity_card ? <div className="full-width-image-wrap table-responsive info-review-table">
                          <h2 className="align-center">voters identity card</h2>
                          <img style={{borderWidth: '2px', borderColor: 'rgb(102, 102, 102)', borderStyle: 'dashed', borderRadius: '5px', maxWidth: '100%', width:'auto', height:'auto'}} src={Urls.site+this.state.template.photo_voters_identity_card} alt="voters identity card"/>
                      </div> : '' }
                                                                                                        

                    {this.state.template.photo_temp_ownership_certificate ? <div className="full-width-image-wrap table-responsive info-review-table">
                    <h2 className="align-center"> Ownership Certificate of Temporary Residence</h2>
                    <img style={{borderWidth: '2px', borderColor: 'rgb(102, 102, 102)', borderStyle: 'dashed', borderRadius: '5px', maxWidth: '100%', width:'auto', height:'auto'}} src={Urls.site+ this.state.template.photo_temp_ownership_certificate}/>
                  </div> : ''}
                        
                    {this.state.template.photo_temp_bill ? <div className="full-width-image-wrap table-responsive info-review-table">
                            <h2 className="align-center">Bill of Temporary Residence</h2>
                             <img style={{borderWidth: '2px',borderColor: 'rgb(102, 102, 102)', borderStyle: 'dashed', borderRadius: '5px', maxWidth: '100%', width:'auto', height:'auto'}} src={Urls.site+this.state.template.photo_temp_bill}/>
                           </div> : '' }
                                                <div className="full-width-image-wrap table-responsive info-review-table">
                                <div className="row no-margin inline">
                                        <h4 className="align-center">In Case of residing in a rented house</h4>
                                            </div><table className="table">
                                            <tbody>
                                                <tr><td><b>Name of Landlord</b></td> <td>{this.state.template.rented_house_name_of_landlord}</td></tr>
                                                <tr><td><b>Telephone Number</b></td> <td>{this.state.template.rented_house_telephone_number}</td></tr>
                                                <tr><td><b>Full Address</b></td> <td>{this.state.template.rented_house_full_address}</td></tr>
                                                <tr><td><b>Otder additional details</b></td> <td>{this.state.template.rented_house_other_additional_details}</td></tr>
                                            </tbody>
                                    
                                </table>
                        </div>
                    {this.state.template.photo_certified_recommendation ? <div className="full-width-image-wrap table-responsive info-review-table">
                            <div className="row no-margin inline">
                                <div className="col-sm-12">
                                <h4 className="align-center">Cerfified Recommendation ( In case of indian citizen)
                                  <div className="col-xs-12 drop-zone">
                                    <div className="" style={{borderWidth: '2px',borderColor: 'rgb(102, 102, 102)', borderStyle: 'dashed', borderRadius: '5px'}}>
                                      <img width="100%" height="auto" src={Urls.site + this.state.template.photo_certified_recommendation}/>
                                    </div>
                                  </div>
                                </h4></div>
                              </div>
                          </div> : '' }
{this.state.template.photo_refugee_identification ? <div className="full-width-image-wrap table-responsive info-review-table">
                                        <div className="col-sm-12">
                                               <h4 className="align-center">Refugee Identification(In Case of Refugee)</h4>
                                              <div className="col-sm-12 drop-zone">
                                                  <div className="" style={{borderWidth: '2px',borderColor: 'rgb(102, 102, 102)', borderStyle: 'dashed', borderRadius: '5px'}}><img width="100%" height="auto" src={Urls.site+ this.state.template.photo_refugee_identification}/></div>
                                            </div>
                                </div> </div> : '' }
                    </div>
              </div>
        );
    }
}


export default Template
