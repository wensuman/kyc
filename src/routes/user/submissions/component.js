import React from 'react';
import Form from '../../../components/form'
import {Urls} from '../../../config/app'
import Ajax from '../../../modules/request'
import Action from '../../../components/message/action'
import Loader from '../../../components/loader'
import $ from 'jquery';
export class Submissions extends React.Component {
    constructor(props) {
        super(props);
        this.componentDidMount = this.componentDidMount.bind(this)
        this.state={template:'',loaded:false};
    }

    componentDidMount(){
        setTimeout(function(){
            $('.ps-scrollbar-y-rail').css('top','0px');
        $('.ps-scrollbar-y').css('top','0px');
    },200)
        
        Ajax().get(Urls.form.template).then((response=>{
            if(response.data.data)
                this.setState({template:response.data.data});
            this.setState({loaded:true});
        }))
    }
    redirect(){
      alert('Form was submitted successfully');
      setTimeout(this.goToHistory.bind(this),2000)
    }

    goToHistory(){
      this.props.router.push('/history')
    }

    render() {
    if(!this.state.loaded){
        return <Loader/>;
    }
    if(this.state.template && this.state.template.verification == 'verified')
        return (
            <div className="transition-item animated fadeIn">
                <Form type="submission" success={this.redirect.bind(this)} postUrl={Urls.form.submission}  />
            </div>
        );
    return <Action header="Submission" message="You will able to access this section once the template has been verified." act={()=>{this.props.router.push('/template')}} actText="Continue" />;
    }
}


export default Submissions
