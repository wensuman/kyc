import {connect} from 'react-redux'

import component from './component'

const mapDispatchToProps = {
};

const mapStateToProps = function (state) {
    return state
}


export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(component)
