import React from 'react';
import Ajax from '../../../modules/request';
import {Urls} from '../../../config/app';
import Axios from 'axios'
import Loader from '../../../components/loader';
import Table from '../../../components/table';
import $ from 'jquery';


export class Log extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      logs: false,
      pagination:{
        links:{}
      }
    };
    this.loadLogs = this.loadLogs.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)
  }
  componentDidMount(){
     setTimeout(function(){
            $('.ps-scrollbar-y-rail').css('top','0px');
        $('.ps-scrollbar-y').css('top','0px');
    },200)
    this.loadLogs()
  }

  loadLogs(url = null) {
    Ajax().get(url ? url : Urls.log).then((response) => {
      this.setState({logs: response.data.data,pagination:response.data.meta.pagination})
    });
  }
  next(){
    this.loadLogs(this.state.pagination.links.next)
  }
  prev(){
    this.loadLogs(this.state.pagination.links.previous)
  }

  render() {
    if (!this.state.logs) {
      return <Loader/>;
    }

    return (
      <div className="transition-item row center animated fadeIn">
        <div className="col-sm-12">
          <div className="card card-stats">
            <div className="card-header card-header-icon" data-background-color="rose">
                  <i className="material-icons">list</i>
            </div>
            <div className="card-content">
              <h3 className="card-title">Your Log Table</h3>
              <Table tbody={this.state.logs.map(function(m){m.ip_address = m.properties.ip;return m;})} th={[{label:'Log Type',value:'log_name'},{label:'Date',value:'created_at'},{label:'Ip address',value:'ip_address'},{ label:'Description',value:'description'}]} />
            <div className="kyc-pagination">
              { this.state.pagination.links.previous ?    <button className="btn btn-rose btn-fill" disabled={!this.state.pagination.links.previous} onClick={this.prev.bind(this)}>Prev</button> : ""}
              { this.state.pagination.links.next ? <button className="btn btn-rose btn-fill" disabled={!this.state.pagination.links.next} onClick={this.next.bind(this)}>Next</button> : "" }
              </div>
            </div>
          </div>
      </div>
    </div>
    );
  }
}

export default Log
