import React from 'react';
import {IndexLink, Link} from 'react-router'
import './style/dashboard.css'
import {Urls} from '../../../config/app'
import Ajax from '../../../modules/request'
import profileImage from './images/avatar.png'
import Table from '../../../components/table'
import Loader from '../../../components/loader';
import $ from 'jquery';

export default class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pic: '',
      logs: [],
      history: [],
      loaded:false
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount() {
     setTimeout(function(){
            $('.ps-scrollbar-y-rail').css('top','0px');
          $('.ps-scrollbar-y').css('top','0px');
    },200)
    Ajax().get(Urls.form.template).then((response) => {
      if(response.data.data)
        this.setState(response.data.data)
      this.setState({loaded:true})
    })
    Ajax().get(Urls.profile).then((response) => {
      if(response.data.data ){
        this.setState(response.data.data)
      }
    })
    Ajax().get(Urls.log).then((response) => {
      this.setState({logs: response.data.data.filter((map,index) => { return map.log_name != 'login'; }).filter((map,index)=>{return index < 3})})
    });
    Ajax().get(Urls.form.history).then((response) => {
      this.setState({history: response.data.data})
    });
  }

  attribute(history, index) {
    return <tr key={index}>
      <td>{history.id}</td>
      <td>{history.bank_name}</td>
      <td>{history.created_at}</td>
    </tr>
  }

  render() {
    if(!this.state.loaded){
      return <loader/>;
    }
    let htmlFull = (
      <div className="card-content">
        <div>
          <div className="alert alert-info">
            <span>You do not have any submissions made to any bank.</span>
          </div>
        </div>
      </div>
    );
    if(this.state.history.length) {
      const html = this.state.history.map(this.attribute.bind(this))
      htmlFull = (
        <table id="simple-table" className="table">
          <thead>
            <tr>
              <th>Form Id</th>
              <th>Bank Name</th>
              <th>Submission Date</th>
            </tr>
          </thead>
          <tbody>
            {html}
          </tbody>
        </table>
      )
    }
    return (
      <div className="transition-item animated fadeIn">
        <div className="row">
           <div className=" col-xs-12 col-sm-12 col-md-7">
               <div className="card">
              <div className="card-header card-header-icon" data-background-color="rose">
                <i className="material-icons">perm_identity</i>
              </div>
              <div className="card-content">
                <h4 className="card-title">View Profile 
                </h4>
                <form>
                  <div className="row">
                    <div className="col-md-6 userProfileImage">
                      {this.state.photo_person
                          ? <img width="100%" height="auto" src={Urls.site+ this.state.photo_person}/>
                        : <img width="100%" height="auto" src='/profil-pic_dummy.png'/>}
                    </div>
                    <div className="col-md-6">
                      <div className="form-group label-floating ">
                        <label className="control-label">Email</label>
                        <span  className="form-control"  >{this.state.email  ? this.state.email : this.state.permanent_email}</span>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="form-group label-floating">
                        <label className="control-label">Phone Number</label>
                        <span  className="form-control"> {this.state.profile && this.state.profile.phone_number  ? this.state.profile.phone_number : this.state.permanent_phone_number }</span>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-4">
                      <div className='form-group label-floating '>
                        <label className="control-label">First Name</label>
                        <span  className="form-control" >{this.state.first_name}</span>
                      </div>
                    </div>
                    <div className="col-md-4">
                      <div className='form-group label-floating '>
                        <label className="control-label">Middle Name</label>
                        <span   className="form-control">{this.state.middle_name}</span>
                      </div>
                    </div>
                    <div className="col-md-4">
                      <div className='form-group label-floating '>
                        <label className="control-label">Last Name</label>
                        <span   className="form-control">{this.state.last_name}</span>
                      </div>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-md-4">
                      <div className="form-group label-floating">
                        <label className="control-label">Country</label>
                        <span  className='form-control'>{this.state.permanent_country}</span>
                      </div>
                    </div>
                    <div className="col-md-4">
                      <div className="form-group label-floating">
                        <label className="control-label">Zone</label>
                        <span  className='form-control'>{this.state.permanent_zone}</span></div>
                    </div>
                    <div className="col-md-4">
                      <div className="form-group label-floating">
                        <label className="control-label">District</label>
                          <span className='form-control'>{this.state.permanent_district}</span>
                      </div>
                    </div>

                  </div>

                </form>
          </div>
          <div className="card-footer">
            <div className="stats">
              <i className="material-icons">local_offer</i>
             {this.state.verification == 'rejected' ?   <Link to="/template/edit">Edit Your Profile</Link>: ''}
             {this.state.verification == 'pending' ?  'Your profile is currently under verification process.':''}
             {this.state.verification == 'accepted' ? 'Your profile is verified.':''}
            </div>
          </div>
           </div>
        </div>

          <div className="col-md-5 col-sm-12 col-xs-12">

            <div className="col-sm-12 no-padding">
              <div className="card card-stats">
                <div className="card-header" data-background-color="rose">
                    <i className="material-icons">list</i>
                </div>
                <div className="card-content">
                  <h3 className="card-title">Logs</h3>
                  <div className="log-dashboard">
                  <div className='col-sm-12'>
                    <Table tbody={this.state.logs.map(function(m) {
                      m.ip_address = m.properties.ip;
                      return m;
                    })} th={[
                      {
                        label: 'Type',
                        value: 'log_name'
                      }, {
                        label: 'Description',
                        value: 'description'
                      }
                    ]}/>
                  </div>
                  </div>
                </div>
                <div className="card-footer">
                  <div className="stats">
                    <i className="material-icons">date_range</i>
                    <Link to="/logs">
                      View All Logs</Link>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-sm-12 no-padding">
              <div className="card card-stats">
                <div className="card-header" data-background-color="rose">
                  <i className="material-icons">history</i>
                </div>
                <div className="card-content">
                  <h3 className="card-title">History</h3>
                  {htmlFull}
                </div>
                <div className="card-footer">
                  <div className="stats">
                    <i className="material-icons">local_offer</i>
                    <Link to="/history">
                      View Full History</Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
