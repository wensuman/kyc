import {connect} from 'react-redux'

import Dashboard from './component'

const mapDispatchToProps = {
};

const mapStateToProps = function (state) {
    return state
}


export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(Dashboard)
