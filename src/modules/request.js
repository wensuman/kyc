import Axios from 'axios';
import '../config/app';
import Cookie from 'react-cookie'

export function ajax() {

    Axios.defaults.baseURL = 'http://localhost:8000';

    if (Cookie.load('Authorization')){
      Axios.defaults.headers.common['Authorization'] = Cookie.load('Authorization');
    }
    // Axios.defaults.headers.post['Content-Type'] = 'application/json';
    Axios.interceptors.response.use(function (response) {
        return response;
    }, function (error) {
        if(error.response.status === 401){
          Cookie.remove('Authorization');
        }
        let errors = error.response.data.errors;
        for (var i in errors) {
          alert(errors[i][0],'error')
        }
        return error;
    });
    return Axios
}

export default ajax;
