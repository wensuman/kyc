let cri = 'http://api.nepalcoin.com/'

if (__DEV__) {
  cri = 'http://kyc-api.dev/'
}

export const server_uri = cri;
export const server_url = server_uri+'api/'
export const wp_uri = 'http://nepalcoin.com';

export const urler = function (url) {
    return server_url + url + '/'
}

export const urle = function(url){
  return server_url + url
}
export const Urls = {
    site:server_uri,
    main:server_url,
    wp_site_faq: wp_uri+'/faq',
    profile:urler('profile/me'),
    login: urler('auth/login'),
    banks: urler('bank-list'),
    access: urler('access'),
    register:urler('auth/register'),
    tickets: urler('tickets'),
    verify: urler('auth/verify'),
    log: urler('log'),
    upload: urler('upload'),
    forgot_password: urler('forgot-password'),
    dashboard: urler('dashboard'),
    changePassword: urler('change-password'),
    form:{
        template: urler('template'),
        submission: urler('submission'),
        history: urler('history'),
        delete: urler('history')
    }
}



export default server_url;
