import React from 'react'
import {Link} from 'react-router'
import './style.css'

export default class Success extends React.Component {
  render(){
    return (
      <div id='kyc-card' className="animated fadeIn">
        <div id='kyc-upper-side'>
            <h3 id='kyc-status'>
            Success
          </h3>
        </div>
        <div id='kyc-lower-side'>
          <p id='kyc-message'>
            Make sure to recheck your document before submitting to the bank.
          </p>
          <Link to="/submissions" id="kyc-contBtn">Continue</Link>
        </div>
      </div>


    )
  }
}
