import React from 'react'
import {Link} from 'react-router'
import './style.css'

export default class Submission extends React.Component {
  render(){
    return (
      <div id='kyc-card' className="animated fadeIn">
        <div id='kyc-upper-side'>
            <h3 id='kyc-status'>
            Confirmation Requried
          </h3>
        </div>
        <div id='kyc-lower-side'>
          <p id='kyc-message'>
            You are about to submit this kyc form to a bank. If Yes,  press continue.
          </p>
          <Link onClick={this.props.clickHandler} id="kyc-contBtn">Continue</Link>
        </div>
      </div>
    )
  }
}
