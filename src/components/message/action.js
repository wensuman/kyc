import React from 'react'
import {Link} from 'react-router'
import './style.css'

export default class Action extends React.Component {
  render(){
    return (
      <div id='kyc-card' className="animated fadeIn">
        <div id='kyc-upper-side'>
            <h3 id='kyc-status'>
            {this.props.header}
          </h3>
        </div>
        <div id='kyc-lower-side'>
          <p id='kyc-message'>
            {this.props.message}
          </p>
          <button onClick={this.props.act} id="kyc-contBtn">{this.props.actText}</button>
        </div>
      </div>


    )
  }
}
