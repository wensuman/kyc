import React from 'react'
import {Link} from 'react-router'
import './css/style.css'
import userImage from './images/avatar.jpg'
import sidebarImage from './images/sidebar-1.jpg'
import Ajax from '../../modules/request'
import {Urls} from '../../config/app'
import Cookie from 'react-cookie'

export default class Sidebar extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      pic:'',
      first_name:'',
      last_name:'',
      loaded:false
    }
  }

  push(location) {
    this.props.router.push(location)
  }

  
  logout() {
    Cookie.remove('Authorization')
    this.props.router.push('/login')
  }
  render() {
    return (
      <div className="sidebar" data-active-color="rose" data-background-color="black" data-image={sidebarImage}>
        <div className="logo">
          <a href="/" className="simple-text">
            K Y C
          </a>
        </div>
        <div className="logo logo-mini">
          <a href="/" className="simple-text">
            K Y C
          </a>
        </div>
        <div className="sidebar-wrapper">
          <ul className="nav">
            <li className="active">
              <Link to="/" activeClassName="active">
                <i className="material-icons">dashboard</i>
                <p>
                  Dashboard
                </p>
              </Link>
            </li>
            <li>
              <Link to="/template" activeClassName="active">
                <i className="material-icons">receipt</i>
                <p>
                  Template
                </p>
              </Link>
            </li>
            <li>
              <Link to="/submissions" activeClassName="active">
                <i className="material-icons">send</i>
                <p>
                  Submissions
                </p>
              </Link>
            </li>
            <li>
              <Link to="/history" activeClassName="active">
                <i className="material-icons">history</i>
                <p>
                  Histories
                </p>
              </Link>
            </li>
            <li>
              <Link to="/logs" activeClassName="active">
                <i className="material-icons">format_list_bulleted</i>
                <p>
                  Logs
                </p>
              </Link>
            </li>
            
            <li>
              <a target="_blank" href={Urls.wp_site_faq} >
                <i className="material-icons">question_answer</i>
                <p>
                  FAQs
                </p>
              </a>
            </li>

          </ul>
        </div>
      </div>
    )
  }
}

