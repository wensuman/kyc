import React from 'react';
import BrowserRouter from 'react-router'
import Ajax from '../../modules/request';
import {Urls, urle, urler} from '../../config/app';
import {Countries, Districts, Zones} from '../../config/lists';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import request from 'superagent';
import Dropzone from 'react-dropzone';
import './css/form.css';
import './css/style.css';
import Success from '../message/success'
import Action from '../message/action'
import Submission from '../message/submission'
import Calendar from 'react-input-calendar'
import attributes from './attributes'
import Loader from '../loader'

export default class Form extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      activeStep: 1,
      maxSteps: 7,
      change: false,
      loading: true,
      initial_bank:1,
      initial_relations:1
    };
    this.icons = {history:'history',submission:'send',template:'receipt'};
    this.attributes = attributes;
    this.componentDidMount = this.componentDidMount.bind(this)
    this.getBanks = this.getBanks.bind(this)
    this.postData = this.postData.bind(this)
    this.bootJquery = this.bootJquery.bind(this);

  }
  performSubmission() {
    this.submit();
  }
  bootJquery(){
    var th = this;
    setTimeout(function(){
      jQuery('.datepicker').datetimepicker({
            format: 'MM/DD/YYYY',
            icons: {
              time: "fa fa-clock-o",
              date: "fa fa-calendar",
              up: "fa fa-chevron-up",
              down: "fa fa-chevron-down",
              previous: 'fa fa-chevron-left',
              next: 'fa fa-chevron-right',
              today: 'fa fa-screenshot',
              clear: 'fa fa-trash',
              close: 'fa fa-remove',
              inline: true
            }
          }).on('dp.change',(e)=>{
            th.setState({change: true,[e.target.name]:e.target.value});
          });
      },3000);
  }

  componentDidMount() {
    this.bootJquery();     
    if (this.props.type == 'history') {
      this.setState(this.props.stats)
      this.setState({change: false, uploading: false, headerText: 'You submitted a form with following informations:'})
      this.setState({activeStep: 1})
      return;
    }
    Ajax().get(Urls.form.template).then((response) => {
      this.setState({loading: false})
      if (response.data.data) {
        response.data.data.activeStep = 1;
        this.setState(response.data.data)
        this.setState({change: false, uploading: false})
      }
    })
    this.getBanks();
  }

  submitForVerification(){
    this.setState({verification:'pending'});
    setTimeout(()=>{
      this.postData('Your template has been successfully submitted for verification.', this.props.onVerifyRequest);
    },500);
  }

  submit(event) {
    if (this.state.change || this.props.type == 'submission' || this.props.type == 'history') {
      this.postData();      
    }
  }

  postData(message, funct){
    if (!this.state.bank_id && this.props.type == 'submission') {
      alert('Please select a bank to submit this form', 'error');
      return;
    }
    var banksTemp = this.state.banks;
    Ajax().post(this.props.postUrl, {
      data: this.state,
      bank_id: this.state.bank_id
    }).then((response, error) => {
      this.setState({change: false, banks: banksTemp});
      if (response.response && response.response.status == 400) {
        alert('You have already submitted a  kyc form to this bank', 'error');
        return;
      }
      if (response.status == 201) {
        this.props.type == 'template' && alert(message ? message : 'Form has been saved successfully') && funct &&  funct();
        if(funct){
          funct();
        }
        if (this.props.type == 'submission') {
          this.props.success();
          this.setState({submitted: true});
        }
        if(this.props.type=='history'){
          alert('Your form has been successfully submitted.');
          setTimeout(()=>{this.props.success()},3000)
        }
        return;
      }
      alert('Something went wrong. Please try again later.', 'error')
    })
  }

  getBanks() {
    if (!this.state.banks && this.props.type === 'submission') {
      Ajax().get(Urls.banks).then((response) => {
        this.setState({
          banks: response.data.data.map(function(bank) {
            return {label: bank.profile.name, value: bank.id}
          })
        })
      })
    }
  }

  removeImage(index, event) {
    let newArray = this.state.files.slice().filter(function(files, indx) {
      return index != indx
    });

    this.setState({files: newArray});
  }

  prev() {
    if (this.state.activeStep > 1) {
      this.setState({
        activeStep: this.state.activeStep - 1
      })
      this.props.type === 'template'
        ? this.submit()
        : '';
    }

  }
  next() {
    if (this.state.activeStep < this.state.maxSteps)
      this.setState({
        activeStep: this.state.activeStep + 1
      })
    this.props.type === 'template'
      ? this.submit()
      : '';
  }

  multiSelect(name,value){
    this.setState({[name]: value})
  }

  change(name, value, event, multi=false) {
    if(value == true){
      let state = this.state[name].slice();
      state.push(event)
      this.setState({[name]: state})
    }
    if (value && value.target && (value.target.value || value.target.value === '')) {
      this.setState({[name]: value.target.value, change: true});
    } else if (value.label) {
      this.setState({[name]: value.value, change: true})
    } else {
      this.setState({
        change: true,
        [name]: value
          ? value
          : event.target.value
      });
    }
    this.setState({applicant_name: (this.state.first_name ? this.state.first_name : '') + ' ' + (this.state.middle_name ? this.state.middle_name: '') + ' ' + (this.state.last_name ? this.state.last_name:'')})
  }

  onDrop(meta, acceptedFiles, rejectedFiles) {
    const req = request.post(Urls.upload);
    req.attach('file', acceptedFiles[0]);
    this.setState({uploading: true});
    req.end((err, response) => {
      if (err && err.response && err.response.text) {
        let json = JSON.parse(err.response.text)
        alert(json.errors.file[0], 'error')
        this.setState({uploading: false})
      } else
        this.setState({[meta]: response.body.data[0].name, uploading: false, change: true})
    });
  }

  check(name, value, event) {
    if (this.state[name] == 'Yes') {
      this.setState({[name]: 'No', change: true});
    } else {
      this.setState({[name]: 'Yes', change: true});
    }
  }

  changeStep(step) {
    this.setState({activeStep: step})
  }

  render() {
    if ((this.props.type == 'submission' && !this.state.banks) || this.state.loading) {
      return <Loader/>;
    }
    return (
                  <div id="form-template" className={(this.state.verification == 'pending' || this.state.verification ==  'verified') && this.props.type =='history' ? 'card verified' : 'card'}>
                      <div className="card-header card-header-icon" data-background-color="rose">
                         <i className="material-icons">{this.icons[this.props.type]}</i>
                      </div>
                      <div className="card-content">
                        <h3 className="card-title">Enter the following information</h3>
                        {this.state.reason ? <div className="alert alert-danger">{this.state.reason}</div> : ''}
                      </div>
                      <div className="clearfix"></div>
                        {this.props.type === 'submission' 
                        ? 
                        <div className="container-fluid select-bank-section">
                        <div className="col-xs-8 select-bank">
                        <div className="col-sm-12 col-xs-8 no-padding">
                          <label className="control-label ">Select a bank to submit a form</label>
                        </div>
                        <div className="col-sm-12 col-xs-12 no-padding"> 
                        <Select searchable={false} clearable={false} placeholder="Select a bank" value={this.state.bank_id} style={{
                            zIndex: 3
                          }} onChange={this.change.bind(this, 'bank_id')} clearable={false} options={this.state.banks}/>
                         </div>
                      </div>
                       </div>
                      : ''}
                       
                     
                      <div className="step-navigation">
                        <ul className="steps">
                          {this.attributes.steps.map((value, num) => (
                            <li key={num + 1} onClick={num < (this.state.maxSteps - 1)  ? this.changeStep.bind(this, num + 1) : ()=>{}} className={this.state.activeStep > (num + 1)
                              ? 'complete active'
                              : (this.state.activeStep === (num + 1)
                                ? 'active'
                                : '')}>
                              <span className="step">{++num}</span>
                              <span className="title">{value}</span>
                            </li>
                          ))}
                        </ul>
                      </div>
                      <div className="clearfix"></div>
                      <div className="step-content pos-rel">
                        <div className={this.state.activeStep === 1
                          ? "step-pane active"
                          : 'step-pane'}>
                          <div className="card-content step-1">
                            <form method="#" action="#">
                              <div className="col-sm-4 col-xs-12">
                                <div className="col-sm-12 col-xs-12 no-padding">
                                  <label className="control-label ">Title</label>
                                </div>
                                <div className="col-sm-12 col-xs-12 no-padding">
                                <Select searchable={false} clearable={false} placeholder="Select a title" onChange={this.change.bind(this,'title')} value={this.state.title} options={this.attributes.human_titles.map((map)=>{return {label:map, value:map} })} />
                                </div>
                              </div>
                               <div className="col-sm-4 col-xs-12">
                                  <div className="col-sm-12 col-xs-12 no-padding">
                                    <label className="control-label ">Marital Status:</label>
                                  </div>
                                  <div className="col-sm-12 col-xs-12 no-padding">
                                  <Select searchable={false}  clearable={false} options={[{label:'Single',value:'single'},{label:'Married',value:'married'}]} onChange={this.multiSelect.bind(this,'marital_status')} value={this.state.marital_status} />
                                  </div>
                                </div>
                                 <div className="col-sm-4 col-xs-12">
                                  <div className="col-sm-12 col-xs-12 no-padding">
                                    <label className="control-label ">Status:</label>
                                  </div>
                                  <div className="col-xs-12 col-sm-12 no-padding">
                                  <Select searchable={false}  clearable={false} onChange={this.multiSelect.bind(this,'status')} value={this.state.status} placeholder="Select your status" options={this.attributes.resident_types.map((map)=>{return {label:map,map}})}/>
                                  </div>
                                </div>
                            
                              {this.props.type === 'submission' ? this.attributes.identifier.map((obj, index) => (
                                <div className="col-sm-6 col-xs-12" key={index}>
                                  <div className={this.state[obj.state] ? "form-group  label-floating" : "form-group  label-floating is-empty"}>
                                    <label className="control-label">{obj.name}</label>
                                    <input onChange={this.change.bind(this, obj.state)} value={this.state[obj.state]} type="text" className="form-control"/>
                                  </div>
                                </div>
                              )) : ''}
                              {this.attributes.relations.map((obj, index) => (
                                <div className="col-sm-4 col-xs-12" key={index}>
                                  <div className={this.state[obj.state] ? "form-group  label-floating" : "form-group  label-floating is-empty"}>
                                    <label className="control-label">{obj.name}</label>
                                    <input onChange={this.change.bind(this, obj.state)} value={this.state[obj.state]} type="text" className="form-control"/>
                                  </div>
                                </div>
                              ))}

                              {this.attributes.identity_info.map((map, index) => (
                                <div key={index} className="col-sm-4 col-xs-12">
                                  <div className={this.state[map.state] ? "form-group  label-floating" : "form-group  label-floating is-empty"}>
                                    <label className="control-label">{map.name}</label>
                                    <input onChange={this.change.bind(this, map.state)} value={this.state[map.state]} type="text" name="text" className="form-control"/>
                                  </div>
                                </div>
                              ))}

                              <div className="col-sm-4 col-xs-12">
                                <div className={this.state.date_of_birth ? "form-group for-date label-floating" : "form-group for-date label-floating is-empty"}>
                                  <label className="control-label">D.O.B (mm/dd/yy)</label>
                                  <input className="form-control datepicker" name="date_of_birth" onChange={this.change.bind(this, 'date_of_birth')} value={this.state.date_of_birth}/>
                                </div>
                              </div>
                              <div className="clearfix"></div>   
                               <div className="col-sm-6 col-xs-12 half-block-select">
                                <div className="col-sm-6 col-xs-6 no-padding">
                                  <label className="control-label ">Currency</label>
                                <Select searchable={false}  clearable={false} options={[{label:'Nrs',value:'Nrs'},{label:'Other',value:'others'}]} value={this.state.currency} onChange={this.multiSelect.bind(this,'currency')} placeholder="Select a currency"/>
                                </div>
                                <div className="col-sm-6 col-xs-6">
                                  <div className={this.state.other_currency ?"form-group label-floating" : "form-group label-floating is-empty"}>
                                    <label className="control-label">Other currency</label>
                                    <input type="text" disabled={!this.state.currency || ( this.state.currency && this.state.currency.value != "others" )} value={this.state.other_currency}  onChange={this.change.bind(this,'other_currency')}  className="form-control"/>
                                  </div>
                                </div>
                              </div>                           


                              <div className="col-sm-6 col-xs-12 half-block-select">
                                <div className="col-sm-6 col-xs-6 no-padding">
                                  <label className="control-label ">Occupation</label>
                                  <Select searchable={false}  clearable={false}  onChange={this.multiSelect.bind(this,'occupation')} options={this.attributes.employeeTypes.map((map)=>{return {value:map,label:map}})} value={this.state.occupation} />
                                </div>
                                <div className="col-sm-6 col-xs-6">
                                  <div className={this.state.other_occupation ?"form-group label-floating" : "form-group label-floating is-empty"}>
                                    <label className="control-label">Other Occupation</label>
                                    <input type="text" disabled={!this.state.occupation || (this.state.occupation && this.state.occupation.value!="Other")} value={this.state.other_occupation}  onChange={this.change.bind(this,'other_occupation')} name="" className="form-control" />
                                  </div>
                                </div>
                              </div>

                              <div className="col-sm-6 col-xs-12 half-block-select">
                                <div className="col-sm-6 col-xs-6 no-padding">
                                  <label className="control-label ">Total Annual Income                                    
                                  </label>
                                <Select searchable={false}  clearable={false} onChange={this.change.bind(this,'income')} options={this.attributes.income.map((map)=> {return {label:map, value:map}})} value={this.state.income} placeholder="Select your income" />
                                </div>
                                <div className="col-sm-6 col-xs-6">
                                  <div className={this.state.other_income ? "form-group label-floating": "form-group label-floating is-empty"}>
                                    <label className="control-label">Other Income</label>
                                    <input type="text" disabled={!this.state.income || (this.state.income && this.state.income!="more than 500,000")} value={this.state.other_income}  onChange={this.change.bind(this,'other_income')}  name="" className="form-control"/>
                                  </div>
                                </div>
                              </div>
                              <div className="col-sm-6 col-xs-12 half-block-select">
                                <div className="col-sm-6 col-xs-6 no-padding">
                                  <label className="control-label">If self employed<br/>
                                  </label>
                                <Select searchable={false}  clearable={false} multi onChange={this.multiSelect.bind(this,'self_employed')} options={this.attributes.professionTypes.map((map)=>{return {label:map.name, value:map.name}})} value={this.state.self_employed} />
                                </div>
                                <div className="col-sm-6 col-xs-6">
                                  <div className={this.state.other_employement ? "form-group label-floating": "form-group label-floating is-empty"}>
                                    <label className="control-label">Other employement</label>
                                    <input type="text" disabled={this.state.self_employed && JSON.stringify(this.state.self_employed).indexOf('Other') == -1}  value={this.state.other_employement}  onChange={this.change.bind(this,'other_employement')}  name="" className="form-control" />
                                  </div>
                                </div>
                              </div>
                              <div className="clearfix"></div>
                              <div className="col-sm-6 col-xs-12 half-block-select">
                                <div className="col-sm-6 col-xs-6 no-padding">
                                  <label className="control-label">Religion:</label>
                                <Select searchable={false}  clearable={false} onChange={this.multiSelect.bind(this,'religion')} value={this.state.religion} options={this.attributes.religionTypes.map((map)=>{return {label:map,value:map}})} />
                                </div>
                                <div className="col-sm-6 col-xs-6">
                                  <div className={this.state.other_employement ? "form-group label-floating": "form-group label-floating is-empty"}>
                                    <label className="control-label">Other Religion</label>
                                    <input type="text" disabled={!this.state.religion || (this.state.religion && this.state.religion.value!="Other")} value={this.state.other_religion}  onChange={this.change.bind(this,'other_religion')} name="" className="form-control" />
                                  </div>
                                </div>
                              </div>

                              <div className="col-sm-6 col-xs-12 half-block-select">
                                <div className="col-sm-6 col-xs-6 no-padding">
                                  <label className="control-label">Education Qualification:</label>
                                <Select searchable={false}  clearable={false} options={this.attributes.education_level_types.map((map)=>{return {label:map,value:map}})} value={this.state.education} onChange={this.multiSelect.bind(this,'education')}/>
                                </div>
                                <div className="col-sm-6 col-xs-6">
                                  <div className={this.state.other_qualification ? "form-group label-floating": "form-group label-floating is-empty"}>
                                    <label className="control-label">Other Qualification</label>
                                    <input type="text" disabled={!this.state.education || (this.state.education && this.state.education.value!="Other")} name="" value={this.state.other_qualification}  onChange={this.change.bind(this,'other_qualification')} className="form-control" />
                                  </div>
                                </div>
                               </div> 


                              <div className="col-xs-12 col-sm-4">
                                <div className="col-sm-12 col-xs-12 no-padding">
                                  <label className="control-label">Are you availing Debit/Credit Card facility:</label>
                                </div>
                                <div className="clearfix"></div>
                                <div className="col-sm-12 col-xs-12"></div>
                                <Select searchable={false}  clearable={false} onChange={this.change.bind(this,'card_facility')} value={this.state.card_facility} options={[{label:'Yes',value:'Yes'},{label:'No',value:'No'}]}/>
                              </div>

                              <div className="half-block-select" style={{
                                display: this.state.card_facility === 'Yes'
                                  ? 'block'
                                  : 'none'
                              }}>
                                <div className="col-xs-12 col-sm-4 ">
                                  <div className="col-sm-12 col-xs-12 no-padding">
                                    <label className="control-label">Select the type of card:</label>
                                  </div>
                                  <div className="col-xs-12 col-sm-12 no-padding">
                                    <Select searchable={false}  clearable={false} multi options={this.attributes.card_types.map((map)=>{return {label:map,value:map}})} onChange={this.multiSelect.bind(this,'card_required_types')} value={this.state.card_required_types} />
                                  </div>
                                </div>

                                <div className="col-xs-12 col-sm-4">
                                  <div className="col-xs-12 col-sm-12 no-padding">
                                    <div className={this.state.name_of_card_issuance_bank ? 'form-group label-floating' : "form-group label-floating is-empty"}>
                                      <label className="control-label">Name of card Issuance Bank/s</label>
                                      <input onChange={this.change.bind(this, 'name_of_card_issuance_bank')} className="form-control" name="comment" id="comment" value={this.state.name_of_card_issuance_bank} />
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </form>

                          </div>
                        </div>
                        <div className={this.state.activeStep === 2 ? "step-pane active" : 'step-pane'}>
                          <div className="card-content step-2 for-steps">
                            <form form method="#" action="#">
                              <h3 className="align-center">NRN Details</h3>

                              <div className="col-sm-12 col-xs-12">
                                <div className="col-sm-12 col-xs-12 no-padding">
                                  <label className="control-label">Is account of a non resident or a foriegn national?</label>
                                </div>
                                <div className="col-sm-12 col-xs-12 no-padding">
                                <Select searchable={false}  clearable={false} value={this.state.isNrn} onChange={this.change.bind(this,'isNrn')} options={[{label:'Yes',value:'Yes'},{label:'No',value:'No'}]} />
                                </div>
                              </div>
                              <div className={this.state.isNrn != 'Yes'
                                ? 'nrn-details'
                                : ''}>
                                {this.attributes.nrn_info.map((map, index) => (
                                  <div key={index} className="col-sm-4 col-xs-12">
                                    <div className={this.state[map.state] ? "form-group for-date label-floating" : "form-group for-date label-floating is-empty"}>
                                    <label className="control-label">{map.name}</label>
                                      <input className="form-control datepicker" name={map.state} onChange={this.change.bind(this, map.state)} value={this.state[map.state]}/>
                                    </div>
                                  </div>
                                ))}

                               <div className="half-block-select">
                                <div className="col-sm-6 col-xs-12 select-height">
                                  <div className="col-sm-12 col-xs-12 no-padding">
                                    <label className="control-label">Select a country</label>
                                  </div>
                                  <div className="col-sm-12 col-xs-12 no-padding">
                                    <Select searchable={false}  clearable={false} className="selectpicker" data-style="select-with-transition" multiple   onChange={this.change.bind(this, 'nrn_country')} value={this.state.nrn_country} clearable={false} options={Countries} placeholder="select a country"/>
                                  </div>
                                </div>
                                <div className="col-sm-6 col-xs-12">
                                  <div className={this.state.nrn_visa_no ? "form-group  label-floating":"form-group  label-floating is-empty"}>
                                    <label className="control-label" htmlFor="visa">Visa No</label>
                                    <input onChange={this.change.bind(this, 'nrn_visa_no')} value={this.state.nrn_visa_no} type="text" id="visanumber" className="form-control"/>
                                  </div>
                                </div>
                                </div>
                                <div className="col-xs-12 no-padding">
                                  <h4 className="align-center"><br />Contact Person (In Nepal)</h4>
                                </div>

                                 <div className="col-sm-6 col-xs-12 select-height">
                                  <div className="col-sm-12 col-xs-12 no-padding">
                                    <label className="control-label">Select a Zone</label>
                                  </div>
                                  <div className="col-sm-12 col-xs-12 no-padding">
                                    <Select searchable={false}  clearable={false} className="selectpicker" data-style="select-with-transition" multiple  value={this.state.contact_person_zone} onChange={this.change.bind(this, 'contact_person_zone')} clearable={false} options={Zones} placeholder="select a zone"/>
                                  </div>
                                </div>
                                <div className="col-sm-6 col-xs-12 select-height">
                                  <div className="col-sm-12 col-xs-12 no-padding">
                                    <label className="control-label">Select a district</label>
                                  </div>
                                  <div className="col-sm-12 col-xs-12 no-padding">
                                    <Select searchable={false}  clearable={false} className="selectpicker" data-style="select-with-transition" multiple title="Choose City"  placeholder="select a districts" value={this.state.contact_person_district} onChange={this.change.bind(this, 'contact_person_district')} clearable={false} options={Districts[this.state.contact_person_zone]
                                      ? Districts[this.state.contact_person_zone]
                                      : []}/>
                                  </div>
                                </div>
  
                                <div className="col-md-3 col-sm-4 col-xs-12">
                                  <div className={this.state.contact_person_name ? "form-group  label-floating":"form-group  label-floating is-empty"}>
                                    <label className="control-label" htmlFor="name">Name</label>
                                    <input type="text" onChange={this.change.bind(this, 'contact_person_name')} value={this.state.contact_person_name} name="text" id="contact-name" className="form-control"/>
                                  </div>
                                </div>

                                <div className="col-md-3 col-sm-4 col-xs-12">
                                  <div className={this.state.contact_person_address ? "form-group  label-floating":"form-group  label-floating is-empty"}>
                                    <label className="control-label" htmlFor="address">Address</label>
                                    <input type="text" name="text" onChange={this.change.bind(this, 'contact_person_address')} value={this.state.contact_person_address} id="contact-address" className="form-control"/>
                                  </div>
                                </div>

                                {this.attributes.contact_person_info.map((map, index) => (
                                  <div key={index} className="col-md-3 col-sm-4 col-xs-12">
                                    <div className={this.state[map.state] ? "form-group label-floating":"form-group label-floating is-empty"}>
                                      <label className="control-label" htmlFor="location">{map.name}</label>
                                      <input type="text" name="text" id="location" className="form-control" onChange={this.change.bind(this, map.state)} value={this.state[map.state]}/>
                                    </div>
                                  </div>
                                ))}
                              </div>
                            </form>
                          </div>
                        </div>
                        <div className={this.state.activeStep === 3
                          ? "step-pane active"
                          : 'step-pane'}>
                          <div className="card-content step-3 for-steps">
                          <div className="col-x2-12 align-center">
                            <h3>Business Details (For Business)</h3>
                          </div>
                          <div className="col-sm-4 col-xs-12">
                            <div className="col-sm-12 col-xs-12 no-padding">
                              <label className="control-label">Nature of Business
                              </label>
                            </div>
                            <div className="col-sm-12 col-xs-12 no-padding">
                           <Select searchable={false}  clearable={false} multi options={this.attributes.business_types.map((map, index) => {return {label: map.name,value:map.name}})} onChange={this.multiSelect.bind(this,'business_types')} value={this.state.business_types}/>
                            </div>
                          </div>

                          <div className="col-xs-12 col-sm-4">
                            <div className="col-sm-12 col-xs-12 no-padding">
                              <label className="control-label">Total Annual turnover
                              </label>
                            </div>
                            <div className="col-sm-12 col-xs-12 no-padding">
                            <Select searchable={false}  clearable={false} options={[{label:'Upto 25 Lakh',value:'Upto 25 Lakh'}, {label:'Upto 1 crore',value:'Upto 1 crore'}, {label:'Upto 10 crore',value:'Upto 10 crore'}, {label:'More 10 crore', value:'More 10 crore'}]} onChange={this.change.bind(this, 'annual_turn_over')} value={this.state.annual_turn_over} />
                            </div>
                          </div>

                          <div className="col-xs-12 col-sm-4">
                            <div className="col-sm-12 col-xs-12">
                              <label className="control-label">Whether Income Tax Assessee?</label>
                            </div>
                            <div className="col-sm-12 col-xs-12 no-padding">
                            <Select searchable={false}  clearable={false} options={[{label:'Yes',value:'Yes'},{label:'No',value:'No'}]} onChange={this.change.bind(this,'income_tax_assess')} value={this.state.income_tax_assess} placeholder="Choose whether income tax assess" />
                            </div>
                          </div>

                          <div className="income-tax-details col-xs-12 no-padding" style={{
                            display: this.state.income_tax_assess === 'Yes'
                              ? 'inline-block'
                              : 'none'
                          }}>
                            <h4 className="align-center">Please fill up PAN/VAT information</h4>
                            <div className="half-block-select half-block-label">

                            <div className="col-sm-3 col-xs-12">
                              <div className="col-sm-12 col-xs-12 no-padding">
                                <label className="control-label half-block-label-field">Constitution</label>
                              </div>
                              <div className="col-sm-12 col-xs-12 no-padding">
                               <Select  searchable={false}  clearable={false} multi options={this.attributes.business_constitution_types.map((map, index) => {return {label: map.name,value:map.name}})} onChange={this.multiSelect.bind(this,'business_constitution_types')} value={this.state.business_constitution_types}/>
                              </div>
                            </div>
                            {this.attributes.business_info.map((map, index) => (
                              <div key={index} className="col-sm-4 col-xs-12">
                                <div className={this.state[map.state] ? "form-group  label-floating": "form-group  label-floating is-empty" }>
                                  <label className="control-label">{map.name}</label>
                                  <input onChange={this.change.bind(this, map.state)} value={this.state[map.state]} name="pan-details" type="text" className="pan-details form-control"/>
                                </div>
                              </div>
                            ))}
                            <div className="col-xs-12 col-sm-4">
                              <div className={this.state.registered_on ? "form-group for-date label-floating " : "form-group for-date label-floating is-empty"}>
                              <label className="control-label" htmlFor="name">Registered In</label>
                                <input className="form-control datepicker" name='registered_on' onChange={this.change.bind(this, 'registered_on')} value={this.state.registered_on}/>
                              </div>
                            </div>
                            </div>

                            <div className="gap-large"></div>

                            <h4 className="align-center">Whether dealing with any other bank or already have an account with this bank, if yes, please give details *:</h4>
                            
                            <h4 className="align-center">Facilities/services being availed</h4>
                            <div className="gap-large"></div>
                            {Array.apply(null, Array(this.state.initial_bank)).map((map,index)=>(
                                   <div key={index} className="half-block-select half-block-label">
                              <div className="col-sm-6 col-xs-6">
                                <div className={this.state['other_bank_detail_'+index+'_name'] ? "form-group label-floating" : "form-group label-floating is-empty"}>
                                  <label className="control-label">Name of the Bank &amp; Branch</label>
                                  <input onChange={this.change.bind(this,'other_bank_detail_'+index+'_name')} value={this.state['other_bank_detail_'+index+'_name']}  className="form-control" />
                                </div>
                              </div>
                              <div className="col-sm-6 col-xs-6">
                                <div className="col-sm-12 col-xs-12 no-padding">
                                  <label className="control-label half-block-label-field">Select account type</label>
                                </div>
                                <div className="col-sm-12 col-xs-12 no-padding">
                                 <Select searchable={false}  clearable={false} onChange={this.multiSelect.bind(this,'other_bank_detail_'+index+"_details")} value={this.state['other_bank_detail_'+index+"_details"]} multi options={this.attributes.account_type_of_banks.map((map, index) => {return {label: map,value:map};})}/>
                                </div>
                              </div>
                            </div>
                            ))}
                            <button onClick={()=>{if(this.state.initial_bank > 5) {alert('Maximum number of banks reached','error');return;}this.setState({initial_bank: this.state.initial_bank + 1})}} className="pull-left btn btn-primary bank-add-btn">Add another bank</button>
                          </div>
                          </div>
                        </div>
                        <div className={this.state.activeStep === 4
                          ? "step-pane active"
                          : 'step-pane'}>
                          <div className="card-content step-4 for-steps">
                          <div className="permanent">
                            <div className="col-xs-12 align-center">
                              <h3 className="align-center">Permanent Address</h3>
                            </div>

                            <div className="col-sm-4 col-xs-12 select-height">
                            <div className="col-sm-12 col-xs-12 no-padding">
                              <label className="control-label">Select a country
                              </label>
                            </div>
                              <div className="col-sm-12 col-xs-12 no-padding">
                                <Select searchable={false}  clearable={false} className="selectpicker" data-style="select-with-transition" multiple title="Choose City" onChange={this.change.bind(this, 'permanent_country')} value={this.state.permanent_country} options={Countries} placeholder="select a country"/>
                              </div>
                            </div>

                           
                                {this.state.permanent_country == 'Nepal'
                                  ?  <div className="col-sm-4 col-xs-6 select-height">
                                      <div className="col-sm-12 col-xs-12 no-padding" style={{display: this.state.permanent_country == 'Nepal' ? 'block' : 'none'}}>
                                        <label className="control-label">Select a zone
                                        </label>
                                      </div>
                                      <div className="col-sm-12 col-xs-12 no-padding">
                                      <Select searchable={false}  clearable={false} onChange={this.change.bind(this, 'permanent_zone')} value={this.state.permanent_zone} options={Zones}/>
                                      </div>
                                    </div>
                                : 
                                <div className="col-sm-4 col-xs-6 half-block-select">
                                <div className="col-sm-12 col-xs-12 no-padding">
                                <div className= {this.state.permanent_zone ? "form-group label-floating" : "form-group label-floating is-empty"}>
                                <label className="control-label">Zone / State / Province  </label>
                                  <input type="text" onChange={this.change.bind(this,'permanent_zone')} value={this.state.permanent_zone}  className="form-control" />
                                </div>
                                </div>
                                </div>
                                 }
{this.state.permanent_country == 'Nepal'
                                  ?  <div className="col-sm-4 col-xs-6 select-height">
                                      <div className="col-sm-12 col-xs-12 no-padding" style={{display: this.state.permanent_country == 'Nepal' ? 'block' : 'none'}}>
                                        <label className="control-label">Select a district
                                        </label>
                                      </div>
                                      <div className="col-sm-12 col-xs-12 no-padding">
                                     <Select searchable={false}  clearable={false} onChange={this.change.bind(this, 'permanent_district')} value={this.state.permanent_district} options={Districts[this.state.permanent_zone]? Districts[this.state.permanent_zone]: []}/>
                                      </div>
                                    </div>
                                : 
                                <div className="col-sm-4 col-xs-6 half-block-select">
                                <div className="col-sm-12 col-xs-12 no-padding">
                                <div className= {this.state.permanent_zone ? "form-group label-floating" : "form-group label-floating is-empty"}>
                                <label className="control-label">District / County</label>
                                 <input type="text" onChange={this.change.bind(this,'permanent_district')} value={this.state.permanent_district}  className="form-control"/>
                                </div>
                                </div>
                                </div>
                                 }


                            <div className="clearfix"></div>



                            {this.attributes.address_inputs.map((map, index) => (
                              <div key={index} className="col-sm-4 col-xs-12">
                                <div className={this.state['permanent_'+map.state] ? "form-group label-floating" : "form-group label-floating is-empty"}>
                                  <label className="control-label">{map.name}</label>
                                  <input onChange={this.change.bind(this, 'permanent_' + map.state)} value={this.state['permanent_' + map.state]} type="text" className="form-control"/>
                                </div>
                              </div>
                            ))}
                          </div>
                          <div className="temporary col-xs-12 no-padding">
                            <div className="checkbox align-center">
                              <label>
                              <h3>
                                <input onChange={this.check.bind(this, 'temporary_address_same_as_permanent')} checked={this.state.temporary_address_same_as_permanent === 'Yes'} type="checkbox" className="unvalid address ace" value="on"/>
                                <span className="checkbox-material">
                                  <span className="check"></span>
                                </span>
                                Temporary Address Same As Permanent</h3>
                              </label>
                            </div>
                            <div style={{
                              display: this.state.temporary_address_same_as_permanent == 'Yes'
                                ? 'none'
                                : 'block'
                            }}>
                              <div className="col-sm-4 col-xs-12 select-height"> 
                                <div className="col-sm-12 col-xs-12 no-padding">
                                  <label className="control-label">Select a country
                                  </label>
                                </div>
                                <div className="col-sm-12 col-xs-12 no-padding" >
                                  <Select searchable={false}  clearable={false} className="selectpicker" data-style="select-with-transition" multiple title="Choose City" onChange={this.change.bind(this, 'temporary_country')} value={this.state.temporary_country} options={Countries} placeholder="select a country"/>
                                </div>
                              </div>

                              {this.state.temporary_country == 'Nepal'
                                  ?  <div className="col-sm-4 col-xs-6 select-height">
                                      <div className="col-sm-12 col-xs-12 no-padding" style={{display: this.state.temporary_country == 'Nepal' ? 'block' : 'none'}}>
                                        <label className="control-label">Select a zone
                                        </label>
                                      </div>
                                      <div className="col-sm-12 col-xs-12 no-padding">
                                     <Select searchable={false}  clearable={false} onChange={this.change.bind(this, 'temporary_zone')} value={this.state.temporary_zone} options={Zones || []}/>
                                      </div>
                                    </div>
                                : 
                                <div className="col-sm-4 col-xs-6 half-block-select">
                                <div className="col-sm-12 col-xs-12 no-padding">
                                <div className= {this.state.temporary_zone ? "form-group label-floating" : "form-group label-floating is-empty"}>
                                <label className="control-label">Zone</label>
                                 <input type="text" onChange={this.change.bind(this,'temporary_zone')} value={this.state.temporary_zone}  className="form-control"/>
                                </div>
                                </div>
                                </div>
                                 }
                              {this.state.temporary_country == 'Nepal'
                                  ?  <div className="col-sm-4 col-xs-6 select-height">
                                      <div className="col-sm-12 col-xs-12 no-padding" style={{display: this.state.temporary_country == 'Nepal' ? 'block' : 'none'}}>
                                        <label className="control-label">Select a district
                                        </label>
                                      </div>
                                      <div className="col-sm-12 col-xs-12 no-padding">
                                     <Select searchable={false}  clearable={false} onChange={this.change.bind(this, 'temporary_district')} value={this.state.temporary_district} options={Districts[this.state.temporary_zone]
                                      ? Districts[this.state.temporary_zone]
                                      : []}/>
                                      </div>
                                    </div>
                                : 
                                <div className="col-sm-4 col-xs-6 half-block-select">
                                <div className="col-sm-12 col-xs-12 no-padding">
                                <div className= {this.state.temporary_zone ? "form-group label-floating" : "form-group label-floating is-empty"}>
                                <label className="control-label">District</label>
                                 <input type="text" onChange={this.change.bind(this,'temporary_district')} value={this.state.temporary_district}  className="form-control"/>
                                </div>
                                </div>
                                </div>
                                 }
<div className="clearfix"></div>
                              {this.attributes.address_inputs.map((map, index) => (
                                <div key={index} className="col-sm-4 col-xs-12">
                                  <div className={this.state['temporary_'+map.state] ?"form-group label-floating" : "form-group label-floating is-empty"}>
                                    <label className="control-label">{map.name}</label>
                                    <input onChange={this.change.bind(this, 'temporary_' + map.state)} value={this.state['temporary_' + map.state]} type="text" className="form-control"/>
                                  </div>
                                </div>
                              ))}
                            </div>
                          </div>
                          <div className="correspondence col-xs-12 no-padding">
                            <div className="checkbox align-center add-form">
                              <label>
                              <h3>
                                <input onChange={this.check.bind(this, 'correspondence_address_same_as_permanent')} checked={this.state.correspondence_address_same_as_permanent === 'Yes'} type="checkbox" className="unvalid address ace" value="on"/>
                                <span className="checkbox-material">
                                  <span className="check"></span>
                                </span>
                                Correspondence Address Same As Permanent </h3>
                              </label>
                            </div>
                            <div style={{
                              display: this.state.correspondence_address_same_as_permanent == 'Yes'
                                ? 'none'
                                : 'block'
                            }}>
                              <div className="col-sm-4 col-xs-12 select-height">
                                <div className="col-sm-12 col-xs-12 no-padding">
                                  <label className="control-label">Select a country
                                  </label>
                                </div>                                  
                                <div className="col-sm-12 col-xs-12 no-padding">
                                  <Select searchable={false}  clearable={false} className="selectpicker" data-style="select-with-transition" multiple title="Choose City" onChange={this.change.bind(this, 'correspondence_country')} value={this.state.correspondence_country} options={Countries} placeholder="select a country"/>
                                </div>
                              </div>
                             {this.state.correspondence_country == 'Nepal'
                                  ?  <div className="col-sm-4 col-xs-6 select-height">
                                      <div className="col-sm-12 col-xs-12 no-padding" style={{display: this.state.correspondence_country == 'Nepal' ? 'block' : 'none'}}>
                                        <label className="control-label">Select a zone
                                        </label>
                                      </div>
                                      <div className="col-sm-12 col-xs-12 no-padding">
                                     <Select searchable={false}  clearable={false} onChange={this.change.bind(this, 'correspondence_zone')} value={this.state.correspondence_zone} options={Zones || []}/>
                                      </div>
                                    </div>
                                : 
                                <div className="col-sm-4 col-xs-6 half-block-select">
                                <div className="col-sm-12 col-xs-12 no-padding">
                                <div className= {this.state.correspondence_zone ? "form-group label-floating" : "form-group label-floating is-empty"}>
                                <label className="control-label">Zone</label>
                                 <input type="text" onChange={this.change.bind(this,'correspondence_zone')} value={this.state.correspondence_zone}  className="form-control"/>
                                </div>
                                </div>
                                </div>
                                 }
                              {this.state.correspondence_country == 'Nepal'
                                  ?  <div className="col-sm-4 col-xs-6 select-height">
                                      <div className="col-sm-12 col-xs-12 no-padding" style={{display: this.state.correspondence_country == 'Nepal' ? 'block' : 'none'}}>
                                        <label className="control-label">Select a district
                                        </label>
                                      </div>
                                      <div className="col-sm-12 col-xs-12 no-padding">
                                     <Select searchable={false}  clearable={false} onChange={this.change.bind(this, 'correspondence_district')} value={this.state.correspondence_district} options={Districts[this.state.correspondence_zone]
                                      ? Districts[this.state.correspondence_zone]
                                      : []}/>
                                      </div>
                                    </div>
                                : 
                                <div className="col-sm-4 col-xs-6 half-block-select">
                                <div className="col-sm-12 col-xs-12 no-padding">
                                <div className= {this.state.permanent_zone ? "form-group label-floating" : "form-group label-floating is-empty"}>
                                <label className="control-label">District</label>
                                 <input type="text" onChange={this.change.bind(this,'permanent_district')} value={this.state.permanent_district}  className="form-control"/>
                                </div>
                                </div>
                                </div>
                                 }
<div className="clearfix"></div>
                              {this.attributes.address_inputs.map((map, index) => (
                                <div key={index} className="col-sm-4 col-xs-12">
                                  <div className={this.state['correspondence_'+map.state] ?"form-group label-floating" : "form-group label-floating is-empty"}>
                                    <label className="control-label">{map.name}</label>
                                    <input onChange={this.change.bind(this, 'correspondence_' + map.state)} value={this.state['correspondence_' + map.state]} type="text" className="form-control"/>
                                  </div>
                                </div>
                              ))}
                            </div>
                          </div>
                          </div>
                        </div>
                        <div className={this.state.activeStep === 5
                          ? "step-pane active"
                          : 'step-pane'}>
                          <div className="card-content step-5">
                          <div className="col-xs-12">
                            <div className="gap-large"></div>
                            <h3 className="align-center">Additional Details of Family Members</h3>
                            <div className="">
                            {Array.apply(null, Array(this.state.initial_relations)).map((map,index)=>(
                              <div key={index} className="half-block-select half-block-label">
                                <div className="col-sm-6 col-xs-12">
                                  <div className="col-sm-12 col-xs-12 no-padding">
                                    <label className="control-label half-block-label-field">Relation</label>
                                  </div>
                                  <div className="col-sm-12 col-xs-12 no-padding">
                                   <Select searchable={false}  clearable={false} options={this.attributes.family.map((map, index) => {return {label: map.name,value:map.name}})} onChange={this.multiSelect.bind(this,'relation_'+index+'_type')} value={this.state['relation_'+index+'_type']}/>
                                  </div>
                                </div>
                                <div className="col-sm-6 col-xs-12">
                                  <div className={this.state['relation_'+index+'_name'] ? "form-group label-floating" :  "form-group label-floating is-empty"}>
                                    <label className="control-label">Full Name</label>
                                    <input onChange={this.change.bind(this,'relation_'+index+'_name')} value={this.state['relation_'+index+'_name']} className="form-control" />
                                  </div>
                                </div>
                                <div className="clearfix"></div>
                                <div className="col-sm-4 col-xs-12">
                                  <div className={this.state['relation_'+index+'_citizenship_no'] ? "form-group label-floating" :  "form-group label-floating is-empty"}>
                                    <label className="control-label">Citizenship No.</label>
                                    <input onChange={this.change.bind(this,'relation_'+index+'_citizenship_no')} value={this.state['relation_'+index+'_citizenship_no']} className="form-control" />
                                  </div>
                                </div>
                                <div className="col-sm-4 col-xs-12">
                                  <div className={this.state['relation_'+index+'_issued_by'] ? "form-group label-floating" :  "form-group label-floating is-empty"}>
                                    <label className="control-label">Issued By</label>
                                    <input onChange={this.change.bind(this,'relation_'+index+'_issued_by')} value={this.state['relation_'+index+'_issued_by']} className="form-control" />
                                  </div>
                                </div>
                                <div className="col-sm-4 col-xs-12">
                                  <div className={this.state['relation_'+index+'_issued_on'] ? "form-group label-floating" :  "form-group label-floating"}>
                                    <label className="control-label">Issued On</label>
                                    <input  onChange={this.change.bind(this,'relation_'+index+'_issued_on')} name={'relation_'+index+'_issued_on'} value={this.state['relation_'+index+'_issued_on']} className={"form-control datepicker relator" +index} />
                                  </div>
                                </div>
                              </div>
                              ))}
                             
                              <button onClick={()=>{this.bootJquery(); this.setState({initial_relations: this.state.initial_relations+1})}} className="btn btn-primary pull-left relation-add-btn">Add another relation</button>
                            </div>
                          </div>
                          </div>
                        </div>
                        <div className={this.state.activeStep === 6
                              ? "step-pane active"
                              : 'step-pane'}>
                              {this.state.uploading
                                ? <div>
                                    Uploading ..
                                  </div>
                                : ''}
                                <div className="card-content step-6">
                              <div className="row" style={{
                                display: this.state.uploading
                                  ? 'none'
                                  : 'block'
                              }}>
                                <div className="container">

                                <h4 className="align-center">In case of permanent address</h4>
                                {this.attributes.identification_document_types.map((map, index) => (
                                  <div className='col-lg-4 col-md-6 col-sm-6 col-xs-12 drop-zone' key={index}>
                                  <h5>{map.name}</h5>
                                  <Dropzone style={{display:'block',width: '200px', height:'200px', borderWidth: '2px', borderColor: 'rgb(102, 102, 102)', borderStyle: 'dashed', borderRadius: '5px'}} multiple={false} onDrop={this.onDrop.bind(this, 'photo_' + map.state)}>
                                    {this.state['photo_' + map.state]
                                      ? <div><a onClick={(event)=>{event.stopPropagation();this.setState({['photo_' + map.state]:''})}} className="close-action"><i className="material-icons">close</i></a><img width="100%" height="auto" src={Urls.site + this.state['photo_' + map.state]}/></div>
                                      : 'Drag and drop a photo'}
                                  </Dropzone>
                                  </div>
                                ))}

                               <h4 className="align-center">In case of temporary address</h4>

                                {this.attributes.temporary_document_types.map((map, index) => (
                                  <div className='col-sm-6 col-xs-12 drop-zone' key={index}>
                                    <h5>{map.name}</h5>
                                    <Dropzone  style={{display:'block',width: '200px',height:'300px', borderWidth: '2px', borderColor: 'rgb(102, 102, 102)', borderStyle: 'dashed', borderRadius: '5px'}} multiple={false} onDrop={this.onDrop.bind(this, 'photo_' + map.state)}>
                                      {this.state['photo_' + map.state]
                                        ? <div><a onClick={(event)=>{event.stopPropagation();this.setState({['photo_' + map.state]:''})}} className="close-action"><i className="material-icons">close</i></a><img width="100%" height="auto" src={Urls.site + this.state['photo_' + map.state]}/></div>
                                        : 'Drop a photo of ' + map.name + '  here'}
                                    </Dropzone>
                                  </div>
                                ))}
                                <div className="no-margin">
                                <h4 className="align-center">In case of residing in a rented house</h4>
                                <div className="col-xs-12 col-sm-6">
                                  <div className={this.state.rented_house_name_of_landlord ? "form-group  label-floating ": "form-group  label-floating is-empty"}>
                                  <label className="control-label">Name of Landlord</label>
                                  <input type="text" onChange={this.change.bind(this,'rented_house_name_of_landlord')} value={this.state.rented_house_name_of_landlord} className="form-control" />
                                  </div>
                                </div>
                                <div className="col-sm-6 col-xs-12">
                                  <div className={this.state.rented_house_telephone_number ? "form-group  label-floating ": "form-group  label-floating is-empty"}>
                                  <label className="control-label">Telephone Number</label>
                                  <input type="text"  onChange={this.change.bind(this,'rented_house_telephone_number')} value={this.state.rented_house_telephone_number} className="form-control" />
                                  </div>
                                </div>
                                <div className="col-sm-6 col-xs-12">
                                  <div className={this.state.rented_house_full_address ? "form-group  label-floating ": "form-group  label-floating is-empty"}>
                                  <label className="control-label"> Full Address</label>
                                  <input onChange={this.change.bind(this,'rented_house_full_address')} value={this.state.rented_house_full_address} type="text" className="form-control" />
                                  </div>
                                </div>
                                <div className="col-sm-6 col-xs-12">
                                  <div className={this.state.rented_house_other_additional_details ? "form-group  label-floating ": "form-group  label-floating is-empty"}>
                                  <label className="control-label">Other additional details</label>
                                  <input onChange={this.change.bind(this,'rented_house_other_additional_details')} value={this.state.rented_house_other_additional_details} type="text" className="form-control" />
                                  </div>
                                </div>
                                </div>
                                <div className="clearfix"></div>
                                <div className="no-margin">
                                <h4 className="align-center">In case of Indian Citizen</h4>
                                {this.attributes.indian_document_types.map((map, index) => (
                                  <div className='col-sm-6 col-xs-12 drop-zone' key={index}>
                                  <h5>{map.name}</h5>
                                    <Dropzone  multiple={false}  style={{display:'block',height:'300px',width: '200px', borderWidth: '2px', borderColor: 'rgb(102, 102, 102)', borderStyle: 'dashed', borderRadius: '5px'}} onDrop={this.onDrop.bind(this, 'photo_' + map.state)}>
                                      {this.state['photo_' + map.state]
                                        ? <div><a onClick={(event)=>{event.stopPropagation();this.setState({['photo_' + map.state]:''})}} className="close-action"><i className="material-icons">close</i></a><img width="100%" height="auto" src={Urls.site + this.state['photo_' + map.state]}/></div>
                                        : 'Drop a photo of ' + map.name + '  here'}
                                    </Dropzone>
                                  </div>
                                ))}
                                <h4 className="align-center">In case of Refugee</h4>
                                {this.attributes.refugee_document_types.map((map, index) => (
                                  <div className='col-sm-6 col-xs-12 drop-zone' key={index}>
                                  <h5>{map.name}</h5>
                                    <Dropzone  style={{display:'block', height:'300px',width: '200px', borderWidth: '2px', borderColor: 'rgb(102, 102, 102)', borderStyle: 'dashed', borderRadius: '5px'}} multiple={false} onDrop={this.onDrop.bind(this, 'photo_' + map.state)}>
                                      {this.state['photo_' + map.state]
                                        ? <div><a onClick={(event)=>{event.stopPropagation();this.setState({['photo_' + map.state]:''})}} className="close-action"><i className="material-icons">close</i></a><img width="100%" height="auto" src={Urls.site + this.state['photo_' + map.state]}/></div>
                                        : 'Drop a photo of ' + map.name + '  here'}
                                    </Dropzone>
                                  </div>
                                ))}
                                </div>
                              </div>
                            </div>
                            </div>

                          </div>
                        <div className={this.state.activeStep === 7
                          ? "step-pane active"
                          : 'step-pane'}>
                          {this.props.type == 'submission' || (this.props.type == 'history')
                            ? <Submission clickable={this.state.verification == 'pending'} clickHandler={this.performSubmission.bind(this)}/>:''}
                              {this.props.type == 'template' ? <Action act={this.submitForVerification.bind(this)} actText="Submit" message="" header="Your profile is completed. Please submit for verification for further assistance."/> : ''}
                        </div>
                        <div className="wizard-actions">
                          <button onClick={this.prev.bind(this)} disabled={this.state.activeStep == 1} className="btn btn-primary">
                            <span className="btn-label">
                              <i className="material-icons">keyboard_arrow_left</i>
                            </span>
                            Prev
                            <div className="ripple-container"></div>
                          </button>

                          <button onClick={this.next.bind(this)} disabled={this.state.activeStep == this.state.maxSteps || (this.state.verification == 'pending' && this.state.activeStep == (this.state.maxSteps - 1) )} className="btn btn-primary">
                            Next
                            <span className="btn-label btn-label-right">
                              <i className="material-icons">keyboard_arrow_right</i>
                            </span>
                            <div className="ripple-container"></div>
                          </button>

                        </div>
                      </div>
                      </div>
                   
    );
  }
}
