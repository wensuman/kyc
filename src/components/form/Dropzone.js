import DropZone from 'react-dropzone'
export default () => {
  return (
    <Dropzone onDrop={this.onDrop.bind(this)}>
      {({isDragActive, isDragReject, acceptedFiles, rejectedFiles}) => {
        if (isDragActive) {
          return "This file is authorized";
        }
        if (isDragReject) {
          return "This file is not authorized";
        }
        return acceptedFiles.length || rejectedFiles.length
          ? `Accepted ${acceptedFiles.length}, rejected ${rejectedFiles.length} files`
          : "Try dropping some files";
      }}
    </Dropzone>

    {this.state.files.length > 0
      ? <div>
          <h2>Uploading {this.state.files.length}
            files...</h2>
          <div>{this.state.files.map((file, index) => <img onClick={this.removeImage.bind(this, index)} height="70px" key={index} src={urle(file.name)}/>)}</div>
        </div>
      : null}

  )
}
