export default {
  steps: [
    'IDENTITY DETAILS',
    'NRN DETAILS',
    'BUSINESS DETAILS',
    'ADDRESS DETAILS',
    'FAMILY DETAILS',
    'DOCUMENTS',
    'FINISH'
  ],
  human_titles: [
    'Mr', 'Mrs', 'Miss', 'Minor', 'Other'
  ],
  card_types: [
    'debit', 'credit', 'master'
  ],
  education_level_types: [
    'Slc', 'Literate', 'Graduate', 'Post Graduate', 'Other'
  ],
  identifier:[
  {
      name: 'Account Number',
      state: 'account_number',
      required:false
    }, {
      name: 'Customer ID',
      state: 'customer_id',
      required:false
    }],
  relations: [
    {
      name: 'First Name',
      state: 'first_name',
      required:true
    },
    {
      name: 'Middle Name',
      state: 'middle_name',
      required:true
    },
    {
      name: 'Last Name',
      state: 'last_name',
      required:true
    }, {
      name: "Fathers Name",
      state: 'fathers_name',
      required:true
    }, {
      name: "Grand Fathers Name",
      state: 'grand_fathers_name',
      required:true
    }, {
      name: "Husband's / Wife's Name",
      state: 'spouse_name',
      required:false
    }, {
      name: "Guardians Name (In case of minor)",
      state: 'guardians_name',
      required:false
    }
  ],
  num_of_banks:[0,1,2,3,4,5],
  account_type_of_banks:[
    'Saving account','Current account','Overdraft','Term Loan','Others'
  ],
  family: [
    {
      name: 'Spouse',
      state: 'spouse'
    }, {
      name: 'Father',
      state: 'father'
    }, {
      name: 'Mother',
      state: 'mother'
    }, {
      name: 'Grandfather',
      state: 'grand_father'
    }, {
      name: 'Grandmother',
      state: 'grand_mother'
    }, {
      name: 'Son',
      state: 'son'
    }, {
      name: 'Daughter',
      state: 'daughter'
    }, {
      name: 'Daughter in Law ',
      state: 'daughter_in_law'
    }
  ],
  employeeTypes: [
    'Salaried Government Employee', 'Retired Government Employee', 'Student', 'Housewife', 'Other'
  ],
  income: [
    'upto 50,000',
    'upto 100,000',
    'upto 150,000',
    'upto 500,000',
    'upto 500,000',
    'more than 500,000'
  ],
  professionTypes: [
    {
      name: 'Medical',
      state: 'employed_medical'
    }, {
      name: 'Engineers',
      state: 'employed_engineers'
    }, {
      name: 'Professor',
      state: 'employed_professor'
    }, {
      name: 'CA',
      state: 'employed_ca'
    }, {
      name: 'Business',
      state: 'employed_business'
    }, {
      name: 'Agriculture',
      state: 'employed_agriculture'
    },
    {
      name:'Others',
      state:'employed_others'
    }
  ],
  identity_info: [
    {
      name: 'Citizenship Number',
      state: 'citizenship',
      required:true
    }, {
      name: 'Nationality',
      state: 'nationality',
      required:true
    }, {
      name: 'Passport Number',
      state: 'passport_number',
      required:false
    }, {
      name: 'Driving Liscence Number',
      state: 'driving_liscence_number',
      required:false
    }
  ],
  religionTypes: [
    'Hindu', 'Buddhist', 'Islam', 'Christian','Other'
  ],
  nrn_info: [
    {
      name: 'Date of Becoming Nrn',
      state: 'nrn_date_of_becoming_nrn'
    }, {
      name: 'Date of issue',
      state: 'nrn_date_of_issue'
    }, {
      name: 'Validity',
      state: 'nrn_validity'
    }
  ],
  contact_person_info: [
    {
      state: 'contact_person_municipality',
      name: 'Municipality/VDC'
    }, {
      state: 'contact_person_ward_no',
      name: 'Ward No'
    }, {
      state: 'contact_person_phone_no',
      name: 'Phone Number'
    }, {
      state: 'contact_person_mobile_number',
      name: 'Mobile Number'
    }, {
      state: 'contact_person_relationship_to_me',
      name: 'Relationship To Me'
    }, {
      state: 'contact_person_email',
      name: 'Email'
    }
  ],
  business_constitution_types: [
    {
      name: 'Individual',
      state: 'constitution_type_individual'
    }, {
      name: 'Sole prop',
      state: 'constitution_type_sole'
    }, {
      name: 'Partnership',
      state: 'constitution_type_partnership'
    }, {
      name: 'P.LTD',
      state: 'constitution_type_p_ltd'
    }, {
      name: 'Ltd',
      state: 'constitution_type_ltd'
    }, {
      name: 'Corporation',
      state: 'constitution_type_corporation'
    }, {
      name: 'Corporation',
      state: 'constitution_type_others'
    }
  ],
  business_types: [
    {
      name: 'Trading',
      state: 'business_Trading'
    }, {
      name: 'Industry',
      state: 'business_Industry'
    }, {
      name: 'Service',
      state: 'business_Service'
    }
  ],
  business_info: [
    {
      name: 'Pan No',
      state: 'pan_no'
    }, {
      name: 'Vat No',
      state: 'vat_no'
    }, {
      name: 'Registration No',
      state: 'registered_no'
    }
  ],
  address_main:[
    {
      name: 'Country',
      state: 'country'
    },{
      name: 'Zone / State / Province',
      state: 'zone'
    },{
      name: 'District / County ',
      state: 'district'
    }
  ],
  address_inputs: [
    {
      name: 'Tole / Street Address',
      state: 'tole'
    }, {
      name: 'Block No / Apartment No',
      state: 'block_no'
    }, {
      name: 'P.Box. No',
      state: 'p_box_no'
    }, {
      name: 'City / Municipality / VDC',
      state: 'municipality'
    }, {
      name: 'Ward No / Zip Code / Postal Code',
      state: 'ward_no'
    }, {
      name: 'Email',
      state: 'email'
    }, {
      name: 'Phone Number',
      state: 'phone_number'
    }, {
      name: 'Mobile No',
      state: 'mobile_no'
    }, {
      name: 'Fax No',
      state: 'fax_no'
    }
  ],
  identification_document_types: [
    {
      name: 'You Photo (Passport Size)',
      state: 'person'
    }, {
      state: 'citizenship',
      name: 'Citizenship (Both front and back)'
    }, {
      state: 'liscence',
      name: 'Driving liscence'
    }, {
      state: 'voters_identity_card',
      name: 'Voters Identity card'
    }, {
      state: 'bill',
      name: 'Electricity or Water'
    },{
      state: 'ownership_certificate',
      name: 'House / Land Ownership Certificate'
    }
  ], temporary_document_types: [
    {
      state: 'temp_ownership_certificate',
      name: 'House / Land Ownership Certificate'
    },{
      state:'temp_bill',
      name: 'Water/Electricity bill of current residence'
    }
  ],indian_document_types: [
    {
      state: 'certified_recommendation',
      name: 'Certified Recommendation '
    },{
      state: 'registration_letter_from_indian_embassy',
      name: 'Registration letter from Indian Embassy'
    }
  ],
  refugee_document_types: [
    {
      state: 'refugee_identification',
      name: 'Identification issued by Government of Nepal'
    },{
      state: 'personal_refugee_identification',
      name: 'Personal identification of refugee'
    }
  ],
  resident_types: ['Resident Individual', 'Non Resident NRN', 'Foreign National']
}
