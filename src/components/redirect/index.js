import React from 'react'
import './style.css'

export default class Redirect extends React.Component {

  constructor(props){
    super(props)
  }

  render(){
    return (
        <div className='loader-container'>
          <div className='loader-main'>
            <div className='loader--dot'></div>
            <div className='loader--dot'></div>
            <div className='loader--dot'></div>
            <div className='loader--dot'></div>
            <div className='loader--dot'></div>
            <div className='loader--dot'></div>
            <div className='redirect--text'></div>
          </div>
        </div>
    );
  }
}
