import React from 'react';

export default class Table extends React.Component {
  render() {
    const th = this.props.th.map(function(t, index) {
      return <th key={index}>{t.label}</th>;
    })
    let props = this.props.th;
    const tbody = this.props.tbody
      ? this.props.tbody.map(function(b, index) {
        const td = props.map(function(m, index) {
          return <td  className="animated fadeIn" key={index}>{b[m.value]}</td>;
        })
        return (
          <tr className="animated fadeIn" key={index}>
            {td}
          </tr>
        );
      })
      : [];
    return (
        <div className="table-responsive">
          <table className='table'>
            <thead className="">
              <tr>
                {th}
              </tr>
            </thead>
            <tbody>
              {tbody}
            </tbody>
          </table>
        </div>
    );
  }
}
