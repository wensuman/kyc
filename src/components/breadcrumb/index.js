import React from 'react'
//import './css/style.css'

export default class Breadcrumb extends React.Component {
  constructor(props) {
    super(props)
    this.state = {wantsToQuestion: false};
  }

  toggleQuestionDiv() {
    this.setState({wantsToQuestion: !this.state.wantsToQuestion})
  }

  removeQuestionDiv(){
    this.setState({wantsToQuestion:false})
  }

  render() {
    return (
      <div >
        <div className="kyc-breadcrumbs ace-save-state" id="kyc-breadcrumbs">
          <ul className="kyc-breadcrumb">
            <li>
              <i className="ace-icon fa fa-home home-icon"/>
              <a href="#">Home</a>
            </li>
            {this.props.path.length > 1
              ? <li>{this.props.path.split('/')[0]}</li>
              : ''}
          </ul>
          <div className="pull-right-custom">
            <ul>
              <li  className={this.state.wantsToQuestion ? "purple dropdown-modal open" : "purple dropdown-modal"}>
                <a onClick={this.toggleQuestionDiv.bind(this)} data-toggle="dropdown" className="dropdown-toggle" href="#">
                  <i className="ace-icon fa fa-2x fa-question-circle-o help-icon"/>
                </a>
                <ul className="dropdown-menu-right dropdown-navbar navbar-pink dropdown-menu dropdown-caret dropdown-close">
                  <li className="dropdown-content">
                  <select id="zone" name="zone" className="select2" data-placeholder="Click to Choose...">
                    <option value="">select Bank</option>
                    <option value="Bank Name">Bank Name</option>
                    <option value="Bank Name">Bank Name</option>
                    <option value="Bank Name">Bank Name</option>
                    <option value="Bank Name">Bank Name</option>
                    <option value="Bank Name">Bank Name</option>
                    <option value="Bank Name">Bank Name</option>
                    <option value="Bank Name">Bank Name</option>
                    <option value="Bank Name">Bank Name</option>
                  </select>
                  </li>
                  <li className="dropdown-content">
                   <textarea className="input" name="message" placeholder="Message"></textarea>
                  </li>
                  <li className="dropdown-content">
                   <button className="btn btn-success btn-reset" data-last="reset">
                    Reset
                   </button>
                   <button className="btn btn-success btn-send" data-last="send">
                    send
                   </button>

                  </li>



                </ul>
              </li>
            </ul>
          </div>
        </div>
        <div class="navbar-minimize">
            <button id="minimizeSidebar" class="btn btn-round btn-white btn-fill btn-just-icon">
                <i class="material-icons visible-on-sidebar-regular">more_vert</i>
                <i class="material-icons visible-on-sidebar-mini">view_list</i>
            </button>
        </div>
        <nav className="navbar navbar-transparent navbar-absolute">
              <div className="container-fluid">
                <div className="navbar-header">
                  <a className="navbar-brand" href="#"> Dashboard <div className="ripple-container"></div></a>
                </div>
              </div>
          </nav>
      </div>
    );
  }
}
