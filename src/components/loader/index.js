import React from 'react'
export default class Loader extends React.Component {
  constructor(props){
    super(props)
  }
  render(){
    return (
        <div className='loader-container'>
          <div className='loader-main'>
            <div className='loader--dot'></div>
            <div className='loader--dot'></div>
            <div className='loader--dot'></div>
            <div className='loader--dot'></div>
            <div className='loader--dot'></div>
            <div className='loader--dot'></div>
            <div className='loader--text'></div>
          </div>
        </div>
    );
  }
}
