import React from 'react';
import Cookie from 'react-cookie';
import './css/style.css';
import userIcon from './image/user-icons.png'
//import userImage from './image/avatar2.png'
import {Link} from 'react-router'
import Ajax from '../../modules/request'
import {Urls} from '../../config/app'

export class Nav extends React.Component {
  constructor(props) {
    super(props)
    this.logout = this.logout.bind(this)
    this.state = {
      dropdown: false,
      notificationDropDown: false,
      loaded:false
    }
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount(){
    Ajax().get(Urls.form.template).then((response)=>{this.setState({loaded:true});this.setState(response.data.data ? response.data.data : []);})
  }


  logout() {
    Cookie.remove('Authorization')
    this.props.router.push('/login')
  }

  toggleMenu() {}

  changeDropDown(name) {
    this.setState({
      [name]: !this.state[name]
    })
  }

  removeDropdown(name) {
    this.setState({[name]: false});
  }

  render() {
    return (
      <nav className="navbar navbar-transparent navbar-absolute">
        <div className="container-fluid">
          <div className="navbar-minimize">
            <button id="minimizeSidebar" className="btn btn-round btn-white btn-fill btn-just-icon">
              <i className="material-icons visible-on-sidebar-regular">more_vert</i>
              <i className="material-icons visible-on-sidebar-mini">view_list</i>
            </button>
          </div>
          <div className="navbar-header">
            <button type="button" className="navbar-toggle" data-toggle="collapse">
              <span className="sr-only">Toggle navigation</span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
            </button>
            <a className="navbar-brand" style={{textTransform: 'capitalize'}}>
              {this.props.bread || 'Dashboard'}
            </a>
          </div>
          <div className="collapse navbar-collapse">
            <ul className="nav navbar-nav navbar-right">
              <li className="dropdown">
                <a href="#" className="dropdown-toggle user-toggle" data-toggle="dropdown">
                  {this.state.loaded ? <img className="user-image" src={this.state.photo_person ? Urls.site + this.state.photo_person :userIcon} />
: <i className="fa fa-spin fa-spinner"></i>}
                  <p className="hidden-lg hidden-md">
                    Setting
                    <b className="caret"></b>
                  </p>
                </a>
                <ul className="dropdown-menu">
                  <li>
                    <a >{this.state.applicant_name ? this.state.applicant_name : 'User'} </a>
                  </li>
                  <li>
                    <Link to="/settings" activeClassName="active">
                      <i className=" ace-icon fa fa-user"></i>
                      Settings
                    </Link>
                  </li>
                  <li>
                    <a onClick={this.logout.bind(this)}>
                      <i className="ace-icon fa fa-power-off"></i>
                      Logout
                    </a>
                  </li>

                </ul>
              </li>
              <li className="separator hidden-lg hidden-md"></li>
            </ul>

          </div>
        </div>
      </nav>
    )
  }
}
export default Nav
