PNotify.prototype.options.styling = "bootstrap3";
PNotify.prototype.options.styling = "fontawesome";

function consume_alert() {
  window.alert = function(message, type = 'success') {
    PNotify.removeAll()
    new PNotify({
      text: message,
      type: type,
      styling: 'bootstrap3'
    });
  };
  window.prompt = function(message, success, failure){
    PNotify.removeAll()
    (new PNotify({
    title: 'Confirmation Needed',
    text: 'Are you sure?',
    icon: 'glyphicon glyphicon-question-sign',
    hide: false,
    confirm: {
        confirm: true
    },
    buttons: {
        closer: false,
        sticker: false
    },
    history: {
        history: false
    }
  })).get().on('pnotify.confirm',success).on('pnotify.cancel',failure);
  }
}

consume_alert()

function consume_confirm() {
  PNotify.removeAll()
  window.confirm = function(message, successCallback, errorCallback) {
    (
      new PNotify({
        title: 'Are you sure?',
        text: message,
        icon: 'glyphicon glyphicon-question-sign',
        hide: true,
        confirm: {
          confirm: true
        },
        buttons: {
          closer: false,
          sticker: false
        },
        history: {
          history: false
        }
      })
    ).get().on('pnotify.confirm', successCallback);
  };
}

consume_confirm()


jQuery(document).ready(function($){
  $(document).on('click','.navbar-toggle', function(){
    $('#sidebar').removeClass('menu-min')
    $('#sidebar').toggleClass('mobile')
  })
})
